# trnatools
collection of code to process, analyse and visualise tRNA sequencing experiments

## trna pipeline

* overview

```
$ bash pipeline_trnas.sh --help
tartools, version v0.1.6

Usage: bash pipeline_trnas.sh [options]

Options:
  -h, --help            print help message

  -r, --run             path to sequencing run [required]
  -p, --project         path to project folder [required]
  -s, --sample-sheet    sample sheet csv file  [required]

Bug reports:
  sciclist@brain.mpg.de

```

* run on a compute node

```
$ ssh <user>@lnx-cm-8002.mpibr.local
<user>@lnx-cm-8002:~$ cd /gpfs/schu/data/RNA/Projects/tRNA
<user>@lnx-cm-8002:~$ mkdir tEXXX
# create a screen session to run in the background
<user>@lnx-cm-8002:~$ screen
<user>@lnx-cm-8002:~$ bash trnatools/pipeline_trnas.sh -p /gpfs/schu/data/RNA/Projects/tRNA/tEXXX -r /gpfs/scic/ilmndata/runs/2023/230714_VH00412_62_AACHMVTM5 -s /gpfs/scic/ilmndata/runs/2023/SampleSheets/20230714_EricoL_tRNA_Exp-189/SampleSheet_IEM_tRNA_Exp-189_Tissues_20230714.csv
# press CTRL + A + D to detach screen
[detached from <jobid>.pts-4.lnx-cm-8002]

# to return to the job
<user>@lnx-cm-8002:~$ screen -r <jobid>
```