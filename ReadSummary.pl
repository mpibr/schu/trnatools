#!/usr/bin/perl

use warnings;
use strict;
use File::Basename;

sub parser($$$$);
sub parseLogs($$$);
sub parseBams($$$);
sub tablePrint($);

main:{
    my $pathLogs = shift;
    my $pathBams = shift;

    my %table = ();
    
    parser(\%table, $pathLogs, ".txt", \&parseLogs);
    parser(\%table, $pathBams, ".bam", \&parseBams);

    tablePrint(\%table);

}


sub tablePrint($)
{
    my $table = $_[0];
    my @types = ("raw", "clean",
             "lncRNA", "microRNA", "miscRNA",
             "ncRNA", "rRNA", "mRNA",
             "mt-rRNA", "mt-mRNA",
             "mt-tRNA", "tRNA");
    print "samples", "\t", join("\t", @types), "\n";
    foreach my $sample (sort keys %{$table}) {
        my @counts = ();
        foreach my $tag (@types) {
            my $val = exists($table->{$sample}{$tag}) ? $table->{$sample}{$tag} : 0;
            push(@counts, $val); 
        }
        print $sample, "\t", join("\t", @counts), "\n";
    }
}


sub parser($$$$)
{
    my $table = $_[0];
    my $path = $_[1];
    my $tag = $_[2];
    my $cbk = $_[3];

    foreach my $file (glob "$path/*$tag") {
        my $name = basename($file, $tag);
        my ($sample, $tmp) = split("\_S[0-9]+\_", $name, 2);
        $sample =~ s/_fastp//g;
        $sample =~ s/_rn7rna//g;
        $cbk->($table, $sample, $file);
        #last;
    }

}

sub parseLogs($$$)
{
    my $table = $_[0];
    my $sample = $_[1];
    my $file = $_[2];
    open(my $fh, "grep \"total reads\" $file|") or die $!;
    chomp(my $line_raw = <$fh>);
    chomp(my $line_clean = <$fh>);
    close($fh);
    my $count_raw = ($line_raw =~ m/(\d+)/) ? $1 : 0;
    my $count_clean =  ($line_clean =~ m/(\d+)/) ? $1 : 0;
    $table->{$sample}{"raw"} = $count_raw;
    $table->{$sample}{"clean"} = $count_clean;
}

sub parseBams($$$)
{
    my $table = $_[0];
    my $sample = $_[1];
    my $file = $_[2];

    my %counter = ();

    open(my $fh, "samtools view -q 0 $file |") or die $!;
    while(<$fh>) {
        chomp($_);
        my @bam = split("\t", $_, 12);
        my ($tr, $gn, $tag) = split(";", $bam[2], 3);
        $counter{$tag}{$bam[0]}++;
    }
    close($fh);

    foreach my $tag (keys %counter) {
        my $val = scalar(keys %{$counter{$tag}});
        $table->{$sample}{$tag} = $val;
    }
}
