#!/bin/bash

#SBATCH --nodes=1
#SBATCH --partition=cuttlefish
#SBATCH --time=100:00:00
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=32
#SBATCH --job-name=tRNAs
#SBATCH --error=error_%j.txt
#SBATCH --output=output_%j.txt



# --- helper functions --- #
function echo_err()
{
    echo "$@" 1>&2
}



function print_version()
{
    local git_tag=$(git describe --abbrev=0 --tags)
    local git_count=$(git rev-list HEAD --count)
    echo "${git_tag}.${git_count}"
}



function print_help()
{
    local help_message="
Usage: bash pipeline_trnas.sh [options]

Options:
  -h, --help            print help message

  -r, --run             path to sequencing run [required]
  -p, --project         path to project folder [required]
  -s, --sample-sheet    sample sheet csv file  [required]
  -i, --index           genome index bwt2      [required]

Bug reports:
  sciclist@brain.mpg.de
"
    echo_err "tartools, version $(print_version)"
    echo_err "$help_message"
}



function parse_arguments()
{
    local path_run=""
    local path_project=""
    local sample_sheet=""
    local path_index=""
  
    while [[ $# -gt 0 ]]; do
        case "$1" in
            -h|--help)
                print_help
                return 1
                ;;
            -r|--run)
                path_run="$2"
                shift 2
                ;;
            -p|--project)
                path_project="$2"
                shift 2
                ;;
            -s|--sample-sheet)
                sample_sheet="$2"
                shift 2
                ;;
            -i|--index)
                path_index="$2"
                shift 2
                ;;
            *)
                echo_err "error, unknown option: $1"
                print_help
                return 1
                ;;
        esac
    done

    if [[ -z "$path_run" || -z "$path_project" || -z "$sample_sheet" || -z "$path_index" ]]; then
        echo_err "error, missing required options."
        print_help
        return 1
    fi
  
    if [ ! -d "$path_run" ]; then
        echo_err "error, sequencing run path is not valid."
        print_help
        return 1
    fi
    
    if [ ! -d "$path_project" ]; then
        echo_err "error, project path is not valid."
        print_help
        return 1
    fi
    
    if [ ! -f "$sample_sheet" ]; then
        echo_err "error, sample sheet file is not valid."
        print_help
        return 1
    fi
    
    if [ ! -f ${path_index}.1.bt2 ]; then
        echo_err "error, bowtie2 index is not valid."
        print_help
    fi

    echo "$path_run,$path_project,$sample_sheet,$path_index"
}



# --- configure environment --- #
bcl2fastq=/gpfs/scic/software/biotools/bcl2fastq2/bin/bcl2fastq
fastp=/gpfs/scic/software/biotools/fastp/fastp
path_samtools=/gpfs/scic/software/biotools/htslib/bin
samtools=${path_samtools}/samtools
bowtie2=/gpfs/scic/software/biotools/bowtie2/bowtie2
#bowtie_index=/gpfs/scic/data/genomes/Rat/rn7_Nov2020/rna/bwt2/rn7
reads_summary=/gpfs/schu/data/RNA/Projects/tRNA/trnatools/ReadSummary.pl
reads_counter=/gpfs/schu/data/RNA/Projects/tRNA/trnatools/BamStatsCount.pl

if [ ! -f "$reads_summary" ]; then
    echo_err "error, ReadSummary.pl is missing from pipeline path."
    exit 1
fi

if [ ! -f "$reads_counter" ]; then
    echo_err "error, BamStatsCount.pl is missing from pipeline path."
    exit 1
fi



# --- parse arguments --- #
arguments=$(parse_arguments "$@")
if [[ $? -ne 0 ]]; then
    exit 1
fi
IFS=',' read -r path_run path_project sample_sheet bowtie_index <<< "$arguments"



# --- configure project --- #
project_id=$(basename "$path_project")
path_fastq=${path_project}/fastq
path_fastq_clean=${path_fastq}/clean
path_bams=${path_project}/bams
path_report=${path_project}/report
file_report_summary=${path_report}/tableReport_ReadsSummary_${project_id}.txt
file_report_counts=${path_report}/tableReport_ReadsCount_${project_id}.txt
path_code=${path_project}/code
path_figures=${path_project}/figures



# --- start pipeline --- #
echo $SLURM_SUBMIT_DIR
echo "Running on `hostname`"
current_datetime=$(date +'%Y-%m-%d %H:%M:%S')
echo "pipeline: $current_datetime" > ${path_project}/${project_id}_metadata.txt
echo "project: $path_project" >> ${path_project}/${project_id}_metadata.txt
echo "run: $path_run" >> ${path_project}/${project_id}_metadata.txt
echo "sample sheet: $sample_sheet" >> ${path_project}/${project_id}_metadata.txt
echo "bowtie2 index: $bowtie_index" >> ${path_project}/${project_id}_metadata.txt

# add samtools to PATH
export PATH=${path_samtools}:"$PATH"


# demultiplexing
if [ ! -d "$path_fastq" ]; then
    echo "BCL2FASTQ demultiplexing ..."
    mkdir -p "$path_fastq"

    $bcl2fastq \
    --runfolder-dir "$path_run" \
    --output-dir "$path_fastq" \
    --sample-sheet "$sample_sheet" \
    --use-bases-mask N5Y*,I6 \
    --no-lane-splitting \
    --loading-threads 8 \
    --writing-threads 8 \
    --processing-threads 8 \
    --minimum-trimmed-read-length 0 \
    --mask-short-adapter-reads 0 \
    --barcode-mismatches 0,1
    
    if [[ $? -ne 0 ]]; then
        echo_err "error, bcl2fastq command failed."
        exit 1
    else
        echo "bcl2fastq executed successfully."
    fi
fi



# cleaning
if [ ! -d "$path_fastq_clean" ]; then
    echo "FASTP cleaning ... "
    mkdir -p "$path_fastq_clean"
    
    adapter_1=GATCGGAAGAGCACACGTCTGAACTCCAGTCA
    adapter_2=CTACACGTTCAGAGTTCTACAGTCCGACGATC
    
    for file_fq_r1 in ${path_fastq}/*_R1_*.fastq.gz
    do
        file_name=$(basename $file_fq_r1)
        sample_name="${file_name%%_S[0-9]*_R[0-9]*_001.fastq.gz}"
        [[ "$sample_name" =~ ^Undetermined* ]] && continue

        echo $sample_name

        ## FASTP
        ## trims provided ADAPTERS from the right side only
        ## trims polyA or polyT longer than 10 bases from either side
        ## trims low quality segment on the right side if average qscore is smaller than 20

        $fastp \
        --in1=$file_fq_r1 \
        --out1=${path_fastq_clean}/${sample_name}_clean.fastq.gz \
        --compression=4 \
        --adapter_sequence=$adapter_1 \
        --trim_poly_x \
        --poly_x_min_len 10 \
        --cut_tail \
        --cut_tail_window_size=4 \
        --cut_tail_mean_quality=20 \
        --low_complexity_filter \
        --complexity_threshold=30 \
        --average_qual=15 \
        --length_required=21 \
        --umi \
        --umi_loc=read1 \
        --umi_len=8 \
        --umi_skip=3 \
        --thread=16 \
        --json=${path_fastq_clean}/${sample_name}_fastp.json \
        --html=${path_fastq_clean}/${sample_name}_fastp.html \
        2> ${path_fastq_clean}/${sample_name}_fastp.txt
    
        if [[ $? -ne 0 ]]; then
            echo_err "error, fastp command failed."
            exit 1
        fi
    
        # break
        
    done
fi



# alignment
if [ ! -d "$path_bams" ]; then
    echo "BOWTIE2 alignment ... "
    mkdir -p "$path_bams"
    
    for file_fq in ${path_fastq_clean}/*_clean.fastq.gz
    do
        sample_name=$(basename $file_fq _clean.fastq.gz)
        [[ "$sample_name" =~ ^Undetermined* ]] && continue

        echo $sample_name
        
        $bowtie2 \
        -q \
        -x $bowtie_index \
        -U $file_fq \
        --threads 16 \
        -D 20 -R 3 -N 0 -L 15 -i S,1,0.50 --local \
        --no-unal \
        2> ${path_bams}/${sample_name}_rn7rna.txt | \
        $samtools view -Sb | \
        $samtools sort -@ 16 -o ${path_bams}/${sample_name}_rn7rna.bam -
        $samtools index ${path_bams}/${sample_name}_rn7rna.bam
        
        if [[ $? -ne 0 ]]; then
            echo_err "error, bowtie2 command failed."
            exit 1
        fi
        
        # break
    done
fi



# summary
if [ ! -f "$file_report_summary" ]; then
    echo "collect reads summary ... "
    mkdir -p "$path_report"
    perl $reads_summary "$path_fastq_clean" "$path_bams" > "$file_report_summary"
    
    if [[ $? -ne 0 ]]; then
        echo_err "error, reads summary command failed."
        exit 1
    fi
fi



# counting
if [ ! -f "$file_report_counts" ]; then
    echo "collect counts ... "
    mkdir -p "$path_report"
    perl $reads_counter "$path_bams" "all" > "$file_report_counts"
    
    if [[ $? -ne 0 ]]; then
        echo_err "error, reads counting command failed."
        exit 1
    fi
fi



# code templates
if [ ! -d "$path_code" ]; then
    echo "copy code templates ... "
    mkdir -p "$path_code"
    mkdir -p "$path_figures"
    
    # add a routine to run R code templates
    # to generate quality control figures
fi

echo "all done."
