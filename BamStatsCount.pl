#!/usr/bin/perl

use warnings;
use strict;
use File::Basename;

sub LoadReferenceTable($);
sub ParseBamFile($$$);
sub ParseBaiFile($$$);
sub ParseCigar($);
sub CalcCigarLenRef($);
sub CalcCigarLenQry($);
sub PrintTable($$);
sub PrintTableRaw($$);

main:{
    # input path to BAM files
    my $pathBam = shift;
    my $countsType = shift;
    
    my @listBams = glob("$pathBam/*.bam");
    if (scalar(@listBams) == 0) {
        print "warning: no bam files found.\n";
        return;
    }

    # build reference count table from BAM header
    my $table = LoadReferenceTable($listBams[0]);

    # count BAM files
    my @samples = ();
    foreach my $fileBam (@listBams) {
        my $name = basename($fileBam, ".bam");
        my ($sample, $tmp) = split("\_S[0-9]+\_", $name, 2);
        $sample =~ s/_rn7rna//g;
        push(@samples, $sample);
	    print STDERR $sample,"\n";
        if ($countsType eq "all") {
            ParseBamFile($table, $fileBam, $sample);
        }
        else {
            ParseBaiFile($table, $fileBam, $sample);
        }
        
	    #last;
    }

    # output
    if ($countsType eq "all") {
        PrintTable($table, \@samples);
    }
    else {
        PrintTableRaw($table, \@samples);
    }
}


sub PrintTableRaw($$)
{
    my $table = $_[0];
    my $samples = $_[1];

    # print header
    print "tRNA";
    print "\tlength";
    foreach my $sample (@{$samples}) {
        print "\t",$sample;
    }
    print "\n";

    # print table
    foreach my $trna (sort keys %{$table}) {
        my @line = ();
        push(@line, $trna);
        push(@line, $table->{$trna}{"length"});
        my $total = 0;
        foreach my $sample (@{$samples}) {
            my $valueRaw = exists($table->{$trna}{$sample}{"raw"}) ? $table->{$trna}{$sample}{"raw"} : 0;
            push(@line, $valueRaw);
            $total += $valueRaw;
        }
        print join("\t", @line),"\n" if($total > 0);
    }

}


sub PrintTable($$)
{
    my $table = $_[0];
    my $samples = $_[1];

    # print header
    print "tRNA";
    print "\tlength";
    foreach my $sample (@{$samples}) {
        print "\t",$sample,".raw";
        print "\t",$sample,".charged";
        print "\t",$sample,".uncharged";
    }
    print "\n";

    # print table
    foreach my $trna (sort keys %{$table}) {
        my $line = $trna;
        my $total = 0;
        $line .= "\t" . $table->{$trna}{"length"};
        foreach my $sample (@{$samples}) {
            my $valueRaw = exists($table->{$trna}{$sample}{"raw"}) ? $table->{$trna}{$sample}{"raw"} : 0;
            my $valueCharged = exists($table->{$trna}{$sample}{"charged"}) ? $table->{$trna}{$sample}{"charged"} : 0;
            my $valueUncharged = exists($table->{$trna}{$sample}{"uncharged"}) ? $table->{$trna}{$sample}{"uncharged"} : 0;
            $line .= "\t" . $valueRaw . "\t" . $valueCharged . "\t" . $valueUncharged;
            $total += $valueRaw;
        }
        print $line,"\n" if($total > 0);
    }
}


sub ParseBaiFile($$$)
{
    my $table = $_[0];
    my $fileBam = $_[1];
    my $name = $_[2];
    open (my $fh, "samtools idxstats $fileBam |") or die $!;
    while(<$fh>) {
        chomp($_);
        my @bam = split("\t", $_);
        $table->{$bam[0]}{$name}{"raw"} = $bam[2];
    }
    close($fh);
}


sub ParseBamFile($$$)
{
    my $table = $_[0];
    my $fileBam = $_[1];
    my $name = $_[2];

    #open(my $fh, "samtools view -f 16 $fileBam |") or die $!;
    open(my $fh, "samtools view $fileBam |") or die $!;
    while(<$fh>) {
        chomp($_);
        my @bam = split("\t", $_);
        my $cigar = ParseCigar($bam[5]);
        my $pos = $bam[3];
        my $qryLen = CalcCigarLenQry($cigar);
        my $refLen = CalcCigarLenRef($cigar);
        my $rnaLen = $table->{$bam[2]}{"length"};
        my $isTpEnd = (($pos + $refLen - 1) == $rnaLen) && ($bam[1] & 16);
        $qryLen -= $cigar->[-1][1] if ($cigar->[-1][0] eq "S");
        my $tagSoft = substr($bam[9], $qryLen);
        my $isCharged = $isTpEnd && (($tagSoft =~ m/^CCA/) || ($tagSoft =~ m/CCA$/));
        my $isUncharged = $isTpEnd && !$isCharged;

        $table->{$bam[2]}{$name}{"raw"}++;
        $table->{$bam[2]}{$name}{"charged"}++ if($isCharged);
        $table->{$bam[2]}{$name}{"uncharged"}++ if($isUncharged);
    }
    close($fh);
}


sub ParseCigar($)
{
    my $cigar = $_[0];
    my @table = ();
    for my $tag (grep defined, $cigar =~ /(\d*[MIDNSHP=X])/g) {
        my ($len, $op) = $tag =~ /(\d*)(\D*)/a;
        push(@table, [uc($op), $len]);
    }
    return \@table;
}

sub CalcCigarLenRef($)
{
    my $cigar = $_[0];
    my $lensum = 0;
    foreach my $tag (@{$cigar}) {
        my $op = $tag->[0];
        my $len = $tag->[1];
        $lensum += $len if ($op eq 'M' || $op eq 'D' || $op eq 'N' || $op eq '=' || $op eq 'X');
    }
    return $lensum;
}

sub CalcCigarLenQry($)
{
    my $cigar = $_[0];
    my $lensum = 0;
    foreach my $tag (@{$cigar}) {
        my $op = $tag->[0];
        my $len = $tag->[1];
        $lensum += $len if ($op eq 'M' || $op eq 'I' || $op eq 'S' || $op eq '=' || $op eq 'X');
    }
    return $lensum;
}


sub LoadReferenceTable($)
{
    my $fileBam = $_[0];
    my %table = ();
    open(my $fh, "samtools view -H $fileBam |") or die $!;
    while (<$fh>) {
        chomp($_);
        next if($_ !~ m/^\@SQ/);
        my $key = ($_ =~ m/SN:([^\t]+)/) ? $1 : "<unknown>";
        my $len = ($_ =~ m/LN:([\d]+)/) ? $1 : "0";
        $table{$key}{"length"} = $len;
    }
    close($fh);
    return \%table;
}
