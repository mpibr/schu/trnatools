#!/bin/bash

#SBATCH --nodes=1
#SBATCH --partition=cuttlefish
#SBATCH --time=100:00:00
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=32
#SBATCH --job-name=jRATISOLATION
#SBATCH --error=error_%j.txt
#SBATCH --output=output_%j.txt

echo $SLURM_SUBMIT_DIR
echo "Running on `hostname`"

# data source
STAR=/gpfs/scic/software/biotools/STAR-2.7.9a/bin/Linux_x86_64/STAR
STAR_REFERENCE=/storage/schu/ProjectGroups/RNA/Projects/tRNA/Genome/Rnor6RNA/star
PATH_FASTQ=/gpfs/scic/illuminanextseq/raw_data/2021-Runs/210720_VH00412_12_AAAGT2JHV/Analysis/1/Data/fastq
PATH_RESULT=/gpfs/scic/data/projects/RatIsolation
PATH_BAMS=$PATH_RESULT/bams
PATH_COUNTS=$PATH_RESULT/counts
PATH_LOGS=$PATH_RESULT/logs
mkdir -p $PATH_BAMS
mkdir -p $PATH_LOGS
mkdir -p $PATH_COUNTS

for FQ_L1R1 in $PATH_FASTQ/*_L001_R1_*.fastq.gz
do
    SAMPLE_ID=$(basename $FQ_L1R1)
    SAMPLE_ID=${SAMPLE_ID%%_*}
    FQ_L2R1=$(ls $PATH_FASTQ/${SAMPLE_ID}_*L002_R1_*.fastq.gz)
    FQ_L1R2=$(ls $PATH_FASTQ/${SAMPLE_ID}_*L001_R2_*.fastq.gz)
    FQ_L2R2=$(ls $PATH_FASTQ/${SAMPLE_ID}_*L002_R2_*.fastq.gz)
    echo $SAMPLE_ID

    echo "alignment ..."
    $PATH_STAR/STAR \
    --runMode alignReads \
    --runThreadN 16 \
    --outFileNamePrefix $PATH_BAMS/ \
    --genomeDir $PATH_GENOME \
    --readFilesIn $FQ_L1R1,$FQ_L2R1 $FQ_L1R2,$FQ_L2R2 \
    --readFilesCommand zcat \
    --outSAMattributes All \
    --outStd Log \
    --outSAMtype BAM SortedByCoordinate \
    --outSAMstrandField intronMotif \
    --outFilterIntronMotifs RemoveNoncanonical \
    --alignSoftClipAtReferenceEnds No \
    --outFilterScoreMinOverLread 0.25 \
    --outFilterMatchNminOverLread 0.25 \
    --clip5pNbases 0,3 \
    --quantMode GeneCounts

    echo "index alignment ..."
    mv $PATH_BAMS/Aligned.sortedByCoord.out.bam $PATH_BAMS/$SAMPLE_ID".bam"
    samtools index $PATH_BAMS/$SAMPLE_ID".bam"

    echo "cleanup ..."
    mv $PATH_BAMS/Log.final.out  $PATH_LOGS/$SAMPLE_ID"_seqlog.txt"
    mv $PATH_BAMS/ReadsPerGene.out.tab $PATH_COUNTS/$SAMPLE_ID"_counts.txt" 
    rm -f $PATH_BAMS/SJ.out.tab
    rm -f $PATH_BAMS/Log.out
    rm -f $PATH_BAMS/Log.progress.out

    # break;
done

