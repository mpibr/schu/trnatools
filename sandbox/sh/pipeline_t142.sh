#!/bin/bash

# echo Demultiplex #this step converts the raw sequencing data into Fastq format and splits them according to index used in the Nextera PCR
# 
# bcl2fastq --runfolder-dir /gpfs/scic/illuminanextseq/raw_data/2021-Runs/210727_VH00412_13_AAAJTNNM5/ \
#   --output-dir /storage/schu/ProjectGroups/RNA/Projects/tRNA/tE142 \
#   --use-bases-mask Y8I5Y*,I6 \
#   --no-lane-splitting \
#   --loading-threads 8 \
#   --writing-threads 8 \
#   --minimum-trimmed-read-length 0 \
#   --mask-short-adapter-reads 0 \
#   --sample-sheet /gpfs/scic/illuminanextseq/raw_data/2021-Runs/210727_VH00412_13_AAAJTNNM5/SampleSheet_20210727.csv
# 
# mkdir DeMultiplexed_Fastqs
# mv Stats/ DeMultiplexed_Fastqs/
# mv Reports/ DeMultiplexed_Fastqs/
# mv Undetermined* DeMultiplexed_Fastqs/
# 
# ##############################################################################################################################
# echo ExtractUMI #Remove UMI from beggining of read and append to read name
# 
# mkdir ExtractUMI_Files
# 
# for l in *_R1_001.fastq.gz;
# 
# do
# exp=$(echo "$l"|cut -d "_" -f1)
# number=$(echo "$l"|cut -d "_" -f2)
# sample=$(echo "$l"|cut -d "_" -f3)
# # echo "$exp"_"$number"_"$sample"_R1_001.fastq.gz
# /storage/schu/ProjectGroups/RNA/Projects/Single_dendrite_RNA-seq/Tools/env/bin/umi_tools extract --extract-method=string \
#       --bc-pattern=NNNNNNNN \
#       --stdin ${l} \
#       --stdout "$exp"_"$number"_"$sample"_R1_Extracted.fastq \
#       --read2-in "$exp"_"$number"_"$sample"_R2_001.fastq.gz \
#       --read2-out="$exp"_"$number".fastq \
#       --quality-filter-mask 15 \
#       --quality-encoding phred33 \
#       --log=ExtractUMI_"$exp"_"$number".log
# 
# mv ${l} DeMultiplexed_Fastqs/
# rm "$exp"_"$number"_"$sample"_R1_Extracted.fastq
# mv "$exp"_"$number"_"$sample"_R2_001.fastq.gz DeMultiplexed_Fastqs/
# mv ExtractUMI_"$exp"_"$number".log ExtractUMI_Files/
# done

#############################################################################################################################
# echo QualityFiltering #Remove reads of low complexity reads or containing homopolymers
# 
# mkdir Fastp_Files
# 
# for g in *.fastq;
# 
# do
# /storage/schu/ProjectGroups/RNA/Projects/Single_dendrite_RNA-seq/Tools/fastp -y -x -f 3\
#   -a AGATCGGAAGAGCACACGTCTGAACTCCAGTCA -i ${g} -o QCed.${g}
# mv fastp.html ${g}.html
# mv ${g} ExtractUMI_Files/
# done
# 
# mv *html Fastp_Files/
# mv *json Fastp_Files/
# 
# ##########################################################################################################################
# echo FastqQC
# 
# mkdir FastQC
# 
# /storage/schu/ProjectGroups/RNA/Projects/Single_dendrite_RNA-seq/Tools/FastQC/fastqc *.fastq
# 
# mv *fastqc* FastQC/

# #############################################################################################################################
# echo STAR_Alignment #Align reads to the mouse genome
# mkdir Alignment_Logs/
# mkdir Filtered_Fastq/
# 
# for h in *.fastq;
# 
# do
# sample=$(echo "$h"|cut -d "." -f2)
# 
# STAR --runMode alignReads --runThreadN 10 \
#  --genomeDir /storage/schu/ProjectGroups/RNA/Projects/tRNA/Genome/Rat/tRNA_Rn6_Stardb/ \
#  --readFilesIn ${h} \
#  --outStd Log  \
#  --outSAMtype BAM SortedByCoordinate \
#  --outSAMstrandField intronMotif \
#  --outFilterMultimapNmax 50 \
#  --outFilterScoreMinOverLread 0 \
#  --outFilterMatchNmin 18 \
#  --outFilterMatchNminOverLread 0 \
#  --outFilterMismatchNoverLmax 0.04 \
#  --alignSoftClipAtReferenceEnds No \
#  --alignIntronMax 1;
#  
# mv Aligned.sortedByCoord.out.bam "$sample".bam;
# mv Log.final.out Alignment_"$sample".log.final.out;
# mv Log.out Alignment_"$sample".log.out;
# rm Log.progress.out
# rm -R _STARtmp
# 
# mv ${h} Filtered_Fastq/
# done
# 
# mv *log*out Alignment_Logs/
# rm SJ.out.tab

############################################################################################################################
# echo DeDuplicateUMIs1 #Deduplicate alignments sharing the same UMI
# 
# mkdir AlignedBams/
# 
# for l in *.bam;
# 
# do
# echo ${l}
# samtools index ${l}
# java -jar -Xss1G -Xmx150G /storage/schu/ProjectGroups/RNA/Projects/Single_dendrite_RNA-seq/Tools/UMICollapse/umicollapse.jar bam \
#   -i ${l} \
#   -o DeDup_${l}
# mv ${l} AlignedBams/
# rm ${l}.bai
# done

featureCounts -a /storage/schu/ProjectGroups/RNA/Projects/tRNA/Genome/Rat/FLT_ncRNAs.gtf -o All.counts *bam -M --fraction -s 2
featureCounts -a /storage/schu/ProjectGroups/RNA/Projects/tRNA/Genome/Rat/Charged_tRNAs.gtf -o tRNA_charging.counts *bam -M -O -s 2##########################################################################################################################
echo FeatureCounts

featureCounts -a /storage/schu/ProjectGroups/RNA/Projects/tRNA/Genome/Mouse/Spiked_GRCm38_tRNAs.gtf -o All.counts *bam -M --fraction -s 2
featureCounts -a /storage/schu/ProjectGroups/RNA/Projects/tRNA/Genome/Mouse/Charged_Mouse_tRNAs.gtf -o tRNA_charging.counts *bam -M -O -s 2

featureCounts -a /storage/schu/ProjectGroups/RNA/Projects/tRNA/Genome/Rat/FLT_ncRNAs.gtf -o All.counts *bam -M --fraction -s 2
featureCounts -a /storage/schu/ProjectGroups/RNA/Projects/tRNA/Genome/Rat/Charged_tRNAs.gtf -o tRNA_charging.counts *bam -M -O -s 2

