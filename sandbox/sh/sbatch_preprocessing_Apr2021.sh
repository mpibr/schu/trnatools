#!/bin/bash

#SBATCH --nodes=1
#SBATCH --partition=tchu
#SBATCH --time=100:00:00
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=88
#SBATCH --job-name=SEPIAPRJ
#SBATCH --error=error_%j.txt
#SBATCH --output=output_%j.txt

echo $SLURM_SUBMIT_DIR
echo "Running on `hostname`"

# clean adapters and remove low quality
PATH_PROJECT=/gpfs/scic/data/projects/SepiaTranscriptome
PATH_FQ_RUN1=$PATH_PROJECT/rnadata/fastq/SepiaRnaRun1
PATH_FQ_RUN2=$PATH_PROJECT/rnadata/fastq/SepiaRnaRun2
PATH_FQ_OUT=$PATH_PROJECT/rnadata/sandbox/fastq
FQ_ONE_RUN1=$(ls $PATH_FQ_RUN1/*_R1_*.fastq.gz)
FQ_TWO_RUN1=$(ls $PATH_FQ_RUN1/*_R2_*.fastq.gz)
FQ_ONE_RUN2=$(ls $PATH_FQ_RUN2/*_R1_*.fastq.gz)
FQ_TWO_RUN2=$(ls $PATH_FQ_RUN2/*_R2_*.fastq.gz)

echo "concatenate FASTQ files"
FQ_ONE_RAW1=$PATH_FQ_OUT/SepiaRna_R1_raw1.fastq.gz
FQ_TWO_RAW1=$PATH_FQ_OUT/SepiaRna_R2_raw1.fastq.gz
#time cat $FQ_ONE_RUN1 > $FQ_ONE_RAW1
#time cat $FQ_TWO_RUN1 > $FQ_TWO_RAW1

FQ_ONE_RAW2=$PATH_FQ_OUT/SepiaRna_R1_raw2.fastq.gz
FQ_TWO_RAW2=$PATH_FQ_OUT/SepiaRna_R2_raw2.fastq.gz
#time cat $FQ_ONE_RUN2 > $FQ_ONE_RAW2
#time cat $FQ_TWO_RUN2 > $FQ_TWO_RAW2

# 102497047 + 455897621 = 558394668


echo "clean adapters and quality"
#PATH_CUTADAPT=/gpfs/scic/software/biotools/venv_cutadapt/bin
#FQ_ONE_CLEAN=$PATH_FQ_OUT/SepiaRna_R1_clean.fastq.gz
#FQ_TWO_CLEAN=$PATH_FQ_OUT/SepiaRna_R2_clean.fastq.gz
ADAPTER_FWD=AGATCGGAAGAGCACACGTCTGAACTCCAGTCA
ADAPTER_REV=AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT
#time $PATH_CUTADAPT/cutadapt -j 64 -q 25,25 -m 51 -a $ADAPTER_FWD -A $ADAPTER_REV -o $FQ_ONE_OUT -p $FQ_TWO_OUT $FQ_ONE_RAW $FQ_TWO_RAW

PATH_BBMAP=/gpfs/scic/software/biotools/bbmap
FQ_OUT_MERGED1=$PATH_FQ_OUT/SepiaRna_merged1.fastq.gz
FQ_OUT_MERGED2=$PATH_FQ_OUT/SepiaRna_merged2.fastq.gz

#time $PATH_BBMAP/bbmerge.sh reads=-1 in1=$FQ_ONE_RAW1 in2=$FQ_TWO_RAW1 out=$FQ_OUT_MERGED1 qtrim=t trimq=10 minlength=35 minavgquality=25 usejni=t adapter1=$ADAPTER_FWD adapter2=$ADAPTER_REV
#time $PATH_BBMAP/bbmerge.sh reads=-1 in1=$FQ_ONE_RAW2 in2=$FQ_TWO_RAW2 out=$FQ_OUT_MERGED2 qtrim=t trimq=10 minlength=35 minavgquality=25 usejni=t adapter1=$ADAPTER_FWD adapter2=$ADAPTER_REV

# 76108851 (74.255%) + 334299818 (73.328%) = 410408669 (73.498%)

echo "clean ribosomal & mitochondrial RNA"
PATH_BOWTIE2=/gpfs/scic/software/biotools/bowtie2
INDEX_RRNA=$PATH_PROJECT/templates/rrna/rrna
INDEX_MITO=$PATH_PROJECT/templates/mito/mito
FQ_OUT_RRNA1=$PATH_FQ_OUT/SepiaRna_noRRNA1.fastq.gz
FQ_OUT_RRNA2=$PATH_FQ_OUT/SepiaRna_noRRNA2.fastq.gz
FQ_OUT_MITO1=$PATH_FQ_OUT/SepiaRna_noMITO1.fastq.gz
FQ_OUT_MITO2=$PATH_FQ_OUT/SepiaRna_noMITO2.fastq.gz

#time $PATH_BOWTIE2/bowtie2 -x $INDEX_RRNA -U $FQ_OUT_MERGED1 --threads 32 --very-fast-local --un-gz $FQ_OUT_RRNA1 > /dev/null
#time $PATH_BOWTIE2/bowtie2 -x $INDEX_RRNA -U $FQ_OUT_MERGED2 --threads 32 --very-fast-local --un-gz $FQ_OUT_RRNA2 > /dev/null
#time $PATH_BOWTIE2/bowtie2 -x $INDEX_MITO -U $FQ_OUT_RRNA1 --threads 32 --very-fast-local --un-gz $FQ_OUT_MITO1 > /dev/null
#time $PATH_BOWTIE2/bowtie2 -x $INDEX_MITO -U $FQ_OUT_RRNA2 --threads 32 --very-fast-local --un-gz $FQ_OUT_MITO2 > /dev/null

#echo "merge runs"
#FQ_OUT_PROCESSED=$PATH_FQ_OUT/SepiaRna_processed.fastq.gz
#cat $FQ_OUT_MITO1 $FQ_OUT_MITO2 | awk -F"\t" '{line++;if(line==0){reads++;print "@SORNNA"reads;}else print $0;if(line==4)line=0}' | gzip > $FQ_OUT_PROCESSED

echo "normalize run"
FQ_OUT_NORMED=$PATH_FQ_OUT/SepiaRna_normed.fastq.gz
#time $PATH_BBMAP/bbnorm.sh in=$FQ_OUT_MITO1","$FQ_OUT_MITO2 out=$FQ_OUT_NORMED target=100 min=5


echo "align Sepia Pharaonis"
INDEX_SEPIA=$PATH_PROJECT/templates/sepiapharaonis/bt2/sepo
FQ_NO_TEMPLATE=$PATH_FQ_OUT/SepiaRna_noSEPPH.fastq.gz
PATH_BAM_OUT=$PATH_PROJECT/rnadata/sandbox/bams
BAM_TEMPLATE=$PATH_BAM_OUT/SepiaRna_sepph.bam
#time $PATH_BOWTIE2/bowtie2 -x $INDEX_SEPIA -U $FQ_OUT_MITO1","$FQ_OUT_MITO2 --threads 32 --very-fast-local --no-unal --un-gz $FQ_NO_TEMPLATE | samtools view -bu - | samtools sort -@ 32 -o $BAM_TEMPLATE -
#time samtools index $BAM_TEMPLATE

echo "all done."