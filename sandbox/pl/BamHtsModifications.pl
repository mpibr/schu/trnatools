#!/usr/bin/perl

use warnings;
use strict;
use Bio::DB::HTS;



main:{
    my $fileBam = shift;
    my $fileFa = shift;
    my $fileRegions = shift;

    my $hts = Bio::DB::HTS->new(-bam => $fileBam, -fasta => $fileFa);
    my $fai = Bio::DB::HTS::Fai->load($fileFa);
    
    my %table = ();

    open(my $fh, "<", $fileRegions) or die $!;
    while(<$fh>) {
        chomp($_);
        
        
        my $snp_caller = sub {
            my ($seqid,$pos,$p) = @_;
            my $refbase = $hts->segment($seqid,$pos,$pos)->dna;
            my $total = 0;
            my $different = 0;

            for my $pileup (@$p) {
                my $b     = $pileup->alignment;
                next if ($pileup->indel or $pileup->is_refskip);   

                my $qbase  = substr($b->qseq,$pileup->qpos,1);

                next if $qbase =~ /[nN]/;

                my $qscore = $b->qscore->[$pileup->qpos];
                next unless $qscore > 25;

                
                $total++;
                $different++ if $refbase ne $qbase;
                $table{$pos}{$refbase}{"total"}++;
                $table{$pos}{$refbase}{"qry"}{$qbase}++;
                #print $pos,"\t",$refbase,"\t",$qbase,"\n";

            }
            #if ($total >= 4 && $different/$total >= 0.25) {
            #    push @SNPs,"$seqid:$pos";
            #}
        };

        $hts->pileup($_, $snp_caller);
        for my $key (sort {$a <=> $b} keys %table) {
            for my $baseRef (sort keys %{$table{$key}}) {

                my $total = $table{$key}{$baseRef}{"total"};

                for my $baseQry (sort keys %{$table{$key}{$baseRef}{"qry"}}) {
                    my $value = $table{$key}{$baseRef}{"qry"}{$baseQry};
                    my $ratio = 100*$value/$total;
                    print $key,"\t", $baseRef,"\t",$baseQry,"\t",$ratio,"\n" if($ratio > 1);
                }
            }
        }

        #my $dna_string = $fai->fetch($_);

        last;
    }
    close($fh);

}

