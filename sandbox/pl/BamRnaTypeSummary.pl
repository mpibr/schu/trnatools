#!/usr/bin/perl

use warnings;
use strict;
use File::Basename;

sub parseBams($$);

main:{
    my $path = shift;
    my $tag = ".bam";
    my @types = ("lncRNA", "mRNA", "microRNA", "miscRNA",
                "mtDNA", "ncRNA", "rRNA", "tRNA");
    print "samples", "\t", join("\t", @types), "\n";
    foreach my $file (glob "$path/*$tag") {
        my $name = basename($file, ".bam");
        my ($sample, $tmp) = split("\_", $name, 2);
        my $result = parseBams($file, \@types);

        print $sample, "\t", join("\t", @{$result}), "\n";
        last;
    }
}

sub parseBams($$)
{
    my $file = $_[0];
    my $types = $_[1];
    my %table = ();
    
    open(my $fh, "samtools view -q 1 $file |") or die $!;
    while(<$fh>) {
        chomp($_);
        my @bam = split("\t", $_, 12);
        my ($tr, $gn, $tag) = split(";", $bam[2], 3);
        $table{$tag}{$bam[0]}++;
    }

    my @result = ();
    foreach my $key (sort @{$types}) {
        my $value = exists($table{$key}) ? scalar(keys %{$table{$key}}) : 0;
        push(@result, $value);
    }
    close($fh);
    return \@result;
}