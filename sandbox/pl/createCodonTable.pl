#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long;

my %table_aa = (
    '*', ['Ter', 'Stop_Codon'],
    'A', ['Ala', 'Alanine'],
    'B', ['Asx', 'Aspargine/Aspartic_Acid'],
    'C', ['Cys', 'Cysteine'],
    'D', ['Asp', 'Aspartic_Acid'],
    'E', ['Glu', 'Glutamic_Acid'],
    'F', ['Phe', 'Phenylalanine'],
    'G', ['Gly', 'Glycine'],
    'H', ['His', 'Histidine'],
    'I', ['Ile', 'Isoleucine'],
    'K', ['Lys', 'Lysine'],
    'L', ['Leu', 'Leucine'],
    'M', ['Met', 'Methionine'],
    'N', ['Asn', 'Asparagine'],
    'P', ['Pro', 'Proline'],
    'Q', ['Gln', 'Glutamine'],
    'R', ['Arg', 'Arginine'],
    'S', ['Ser', 'Serine'],
    'T', ['Thr', 'Threonine'],
    'V', ['Val', 'Valine'],
    'W', ['Trp', 'Tryptophan'],
    'Y', ['Tyr', 'Tyrosine'],
    'Z', ['Glx', 'Glutamate/Glutamine'],
    'X', ['Xaa', 'Any_AA']
);

my %table_codon = (
    'TAA', '*', 'TAG', '*', 'TGA', '*', 'GCA', 'A',
    'GCC', 'A', 'GCG', 'A', 'GCT', 'A', 'TGC', 'C',
    'TGT', 'C', 'GAC', 'D', 'GAT', 'D', 'GAA', 'E',
    'GAG', 'E', 'TTC', 'F', 'TTT', 'F', 'GGA', 'G',
    'GGC', 'G', 'GGG', 'G', 'GGT', 'G', 'CAC', 'H',
    'CAT', 'H', 'ATA', 'I', 'ATC', 'I', 'ATT', 'I',
    'AAA', 'K', 'AAG', 'K', 'CTA', 'L', 'CTC', 'L',
    'CTG', 'L', 'CTT', 'L', 'TTA', 'L', 'TTG', 'L',
    'ATG', 'M', 'AAC', 'N', 'AAT', 'N', 'CCA', 'P',
    'CCC', 'P', 'CCG', 'P', 'CCT', 'P', 'CAA', 'Q',
    'CAG', 'Q', 'AGA', 'R', 'AGG', 'R', 'CGA', 'R',
    'CGC', 'R', 'CGG', 'R', 'CGT', 'R', 'AGC', 'S',
    'AGT', 'S', 'TCA', 'S', 'TCC', 'S', 'TCG', 'S',
    'TCT', 'S', 'ACA', 'T', 'ACC', 'T', 'ACG', 'T',
    'ACT', 'T', 'GTA', 'V', 'GTC', 'V', 'GTG', 'V',
    'GTT', 'V', 'TGG', 'W', 'TAC', 'Y', 'TAT', 'Y'
);

sub usage($);
sub parseGeneBankFile($$);
sub printCodonCounts($);


main:{
    my $file_gbk;
    my $help;
    my %table_counts = ();
    
    # set-up input parameters
    Getopt::Long::GetOptions(
    "gbk|g=s" => \$file_gbk,
    "help|h" => \$help
    ) or usage("Error :: invalid command line options");

    parseGeneBankFile(\%table_counts, $file_gbk);
    printCodonCounts(\%table_counts);

}

sub printCodonCounts($)
{
    my $table_counts = $_[0];

    my @header = ();
    my @list_codons = sort keys %table_codon;
    push(@header, "gene", "transcript");
    push(@header, @list_codons);
    print join("\t", @header), "\n";

    foreach my $gene (sort keys %{$table_counts}) {
        foreach my $transcript (sort keys %{$table_counts->{$gene}}) {
            print $gene, "\t", $transcript;
            foreach my $codon (@list_codons) {
                my $value = exists($table_counts->{$gene}{$transcript}{$codon}) ? $table_counts->{$gene}{$transcript}{$codon} : 0;
                print "\t", $value; 
            }
            print "\n";
        }
    }
}


sub parseGeneBankFile($$)
{
    my $table_counts = $_[0];
    my $file_gbk = $_[1];

    # open file to read
    open(my $fh, "gunzip -c $file_gbk |") or die $!;

    # record separator
    local $/ = "//\n";

    my $counter_records = 0;
    my $counter_coding = 0;

    while (my $record = <$fh>) {
        chomp($record);
        $counter_records++;
        next if ($record !~ m/\/protein_id=/);
        my $symbol = ($record =~ m/\/gene=\"(.+)\"/) ? $1 : "<symbol>";
        my $refseq_id = ($record =~ m/LOCUS\s+([ANXY][MPR]\_[0-9]+)/) ? $1 : "<refseq_id>";
        my $cds_start = ($record =~ m/CDS\s+(\d+)/) ? $1 : -1;
        my $cds_end = ($record =~ m/CDS\s+\d+\.\.(\d+)/) ? $1 : -1;
        next if (($cds_start == -1) || ($cds_end == -1));

        # parse sequence
        my $seq_start = index($record, "ORIGIN");
        my $seq_record = substr($record, $seq_start, length($record) - $seq_start);
        $seq_record =~ s/[^acgtn]//g;
        $seq_record =~ tr/[acgtn]/[ACGTN]/;
        next if (length($seq_record) < ($cds_end - $cds_start));

        for (my $k = $cds_start - 1; $k < $cds_end; $k += 3) {
            my $codon = substr($seq_record, $k, 3);
            $table_counts->{$symbol}{$refseq_id}{$codon}++;
        }
        $counter_coding++;
    }
    close($fh);
    print STDERR "parse GenBankFile, records: ", $counter_records,", protein_coding: ", $counter_coding, "\n"; 
}



sub usage($)
{
    my $message = $_[0];
    if (defined $message && length $message)
    {
        $message .= "\n" unless($message =~ /\n$/);
    }
    
    my $command = $0;
    $command =~ s#^.*/##;
    
    print STDERR (
    $message,
    "usage: $command -gbk genebank_file.gbk.gz\n" .
    "description: parse RNA/Protein GenBank file and extracts symbol and RefSeq IDs information\n" .
    "parameters:\n" .
    "-g, --gbk\n" .
    "\tGZIP compressed GenBank file as downloaded from NCBI https://ftp.ncbi.nih.gov/refseq/R_norvegicus/mRNA_Prot/\n" .
    "-h, --help\n" .
    "\tdefine usage\n"
    );
    
    die("\n");
}
