#!/usr/bin/perl

use warnings;
use strict;
use File::Basename;


sub LoadReferenceLength($);
sub WriteBedRegions($$$);
sub ParseDepthStream($$$$);

main:{
    my $pathBam = shift;
    my $nbins = 35;

    my %table = ();
    foreach my $fileBam (glob "$pathBam/*.bam") {
        my $name = basename($fileBam, ".bam");
        my ($sample, $tmp) = split("\_", $name, 2);
        my $tableLen = LoadReferenceLength($fileBam);
        my $fileBed = WriteBedRegions($tableLen, $pathBam, $sample);
        my $result = ParseDepthStream($tableLen, $fileBed, $fileBam, $nbins);
        $table{$sample} = $result;
        system("rm -rf $fileBed");
    }

    my @header = sort keys %table;
    print "index", "\t", join("\t", @header), "\n";
    for (my $idx = 0; $idx < $nbins; $idx++) {
        print $idx;
        foreach my $key (@header) {
            print "\t", $table{$key}->[$idx];
        }
        print "\n";
    }

}


sub ParseDepthStream($$$$)
{
    my $tableLen = $_[0];
    my $fileBed = $_[1];
    my $fileBam = $_[2];
    my $nbins = $_[3];
    
    my @data = ();
    my %tableDepth = ();
    my %tableCount = ();
    open(my $fh, "samtools depth -b $fileBed $fileBam|") or die $!;
    while (<$fh>) {
        chomp($_);
        my ($trna, $pos, $depth) = split("\t", $_, 3);
        push(@data, [$trna, $pos, $depth]);
        if (exists($tableDepth{$trna})) {
            $tableDepth{$trna} += $depth;
            $tableCount{$trna}++;
        }
        else {
            $tableDepth{$trna} = $depth;
            $tableCount{$trna} = 1;
        }
    }
    close($fh);

    # normalize
    my $factor = (1 / $nbins);
    my @result = (0) x $nbins;
    my @counter = (0) x $nbins;
    foreach my $line (@data) {

        my $trna = $line->[0];
        my $pos = $line->[1];
        my $depth = $line->[2];

        my $len = $tableLen->{$trna};
        my $norm = $tableDepth{$trna} / $tableCount{$trna};
        my $rpos = ($pos - 1) / $len;
        my $rdepth = $depth / $norm;
        my $idx = int($rpos / $factor);

        $result[$idx] = $result[$idx] * ($counter[$idx] / ($counter[$idx] + 1)) + $rdepth * (1 / ($counter[$idx] + 1));
        $counter[$idx]++;
    }
    
    return \@result;
}


sub WriteBedRegions($$$)
{
    my $table = $_[0];
    my $path = $_[1];
    my $sample = $_[2];
    my $fileBed = $path . "/" . $sample . ".bed";
    open(my $fh, ">", $fileBed) or die $!;
    foreach my $trna (sort keys %{$table}) {
        print $fh $trna, "\t", 0, "\t", $table->{$trna}, "\t", $trna, "\n";
    }
    close($fh);
    return $fileBed;
}


sub LoadReferenceLength($)
{
    my $fileBam = $_[0];
    my %table = ();
    open(my $fh, "samtools view -H $fileBam | grep \"\;tRNA\"|") or die $!;
    while (<$fh>) {
        chomp($_);
        next if($_ !~ m/^\@SQ/);
        my $key = ($_ =~ m/SN:([^\t]+)/) ? $1 : "<unknown>";
        my $len = ($_ =~ m/LN:([\d]+)/) ? $1 : "0";
        $table{$key} = $len;
    }
    close($fh);
    return \%table;
}

