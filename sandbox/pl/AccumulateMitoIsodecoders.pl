#!/usr/bin/perl

use warnings;
use strict;

main:{
    my $fileCodons = shift;
    my $fileCharged = shift;
    my $fileMtCodons = shift;

    my %table = ();
    open(my $fh, "<", $fileCodons) or die $!;
    while(<$fh>) {
        chomp($_);
        my @line = split("\t", $_);
        my $key = $line[2] . "-" . $line[0];
        $table{$key} = [0, 0];
    }
    close($fh);

    open($fh, "<", $fileCharged) or die $!;
    while(<$fh>) {
        chomp($_);
        my @line = split("\t", $_);
        next if($line[0] eq "tRNA");
        my ($id, $key, $typ) = split(";", $line[0], 3);
        $key =~ s/iMet-CAT/Met-CAT/g;

        my ($aa, $seq) = split("-", $key, 2);
        $seq =~ tr/ACGT/TGCA/;
        my $seqr = reverse($seq);
        $key = $aa . "-" . $seqr;

        if(exists($table{$key})) {
            $table{$key}[0] += $line[2];
        }
        
    }

    open($fh, "<", $fileMtCodons) or die $!;
    while(<$fh>) {
        chomp($_);
        my 
    }

    foreach my $tag (sort keys %table) {
        print $tag, "\t", $table{$tag}[0], "\t", $table{$tag}[1], "\n";
    }


}



