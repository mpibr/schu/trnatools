#!/usr/bin/perl

use warnings;
use strict;
use File::Basename;

sub LoadReferenceLength($$);
sub ParseBamFile($$);
sub ParseCigar($);
sub CalcCigarLenQry($);
sub CalcCigarLenRef($);

main:{
    my $pathBam = shift;
    print "sample\tavg.len.qry\tavg.len.ref\n";
    foreach my $fileBam (glob "$pathBam/*.bam") {
        my $name = basename($fileBam, ".bam");
        my ($sample, $tmp) = split("\_", $name, 2);
        my %tableLength = ();
        LoadReferenceLength(\%tableLength, $fileBam);
        my ($avgLenQry, $avgLenRef) = ParseBamFile(\%tableLength, $fileBam);
        print $sample,"\t",$avgLenQry,"\t",$avgLenRef,"\n";
    }
}

sub CalcCigarLenQry($)
{
    my $cigar = $_[0];
    my $lensum = 0;
    foreach my $tag (@{$cigar}) {
        my $op = $tag->[0];
        my $len = $tag->[1];
        $lensum += $len if ($op eq 'M' || $op eq 'I' || $op eq 'S' || $op eq '=' || $op eq 'X');
    }
    return $lensum;
}

sub CalcCigarLenRef($)
{
    my $cigar = $_[0];
    my $lensum = 0;
    foreach my $tag (@{$cigar}) {
        my $op = $tag->[0];
        my $len = $tag->[1];
        $lensum += $len if ($op eq 'M' || $op eq 'D' || $op eq 'N' || $op eq '=' || $op eq 'X');
    }
    return $lensum;
}


sub ParseCigar($)
{
    my $cigar = $_[0];
    my @table = ();
    for my $tag (grep defined, $cigar =~ /(\d*[MIDNSHP=X])/g) {
        my ($len, $op) = $tag =~ /(\d*)(\D*)/a;
        push(@table, [uc($op), $len]);
    }
    return \@table;
}


sub ParseBamFile($$)
{
    my $table = $_[0];
    my $fileBam = $_[1];

    my $lenRef = 0;
    my $lenQry = 0;
    my $count = 0;

    open(my $fh, "samtools view -F 16 $fileBam|") or die $!;
    while (<$fh>) {
        chomp($_);
        my @bam = split("\t", $_, 12);
        my ($trn, $gen, $typ) = split(";", $bam[2], 3);
        my $strand = ($bam[1] & 16) ? "-" : "+";
        my $lenRead = length($bam[9]);
        my $cigar = ParseCigar($bam[5]);
        $lenQry += CalcCigarLenQry($cigar);
        $lenRef += CalcCigarLenRef($cigar);
        $count++;

        #print $bam[0],"\t",$bam[1],"\t",$strand,"\t",$lenRead,,"\t",$lenQry,"\t",$lenRef,"\n";
    }
    close($fh);
    return ($lenQry/$count, $lenRef/$count);
}


sub LoadReferenceLength($$)
{
    my $table = $_[0];
    my $fileBam = $_[1];
    open(my $fh, "samtools view -H $fileBam|") or die $!;
    while (<$fh>) {
        chomp($_);
        next if($_ !~ m/^\@SQ/);
        my $key = ($_ =~ m/SN:([^\t]+)/) ? $1 : "<unknown>";
        my $len = ($_ =~ m/LN:([\d]+)/) ? $1 : "0";
        $table->{$key} = $len;
    }
    close($fh);
}

