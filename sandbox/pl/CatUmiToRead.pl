#!/usr/bin/perl

use warnings;
use strict;
use Getopt::Long();

sub usage($);
sub readerFastq($);
sub printFastq($);
sub qualToInt($);

main:{
    my $file_fq;
    my $file_umi;
    my $qscore = 15;
    my $help;

    # Set-up parameters
    Getopt::Long::GetOptions(
        "f|fastq=s" =>\$file_fq,
        "u|umi=s" =>\$file_umi,
        "q|qual=i" =>\$qscore,
        "h|help" => \$help
    ) or usage("Error::invalid command line options");

    # define help output
    usage("version 4.0, Feb 2016") if($help);
    
    # define input files
    usage("Error::query FASTQ file name must be specified")
    unless defined $file_fq;

    usage("Error::query UMI file name must be specified")
    unless defined $file_umi;

    if (($qscore < 0) || (40 < $qscore)) {
        print STDERR "Warning: quality score range [0, 40], set to default 15.";
        $qscore = 15;
    }

    my $it_fq = readerFastq($file_fq);
    my $it_umi = readerFastq($file_umi);
    my $records_total = 0;
    my $records_written = 0;
    while (my $fq = $it_fq->()) {
        my $umi = $it_umi->();
        last if(!defined($umi));
        $records_total++;
        my $score = qualToInt($umi->[-1]);
        next if($score < $qscore);
        ## add umi tag to header
        ($fq->[0] =~ s/\s/\:$umi->[1]\ /);
        printFastq($fq);
        $records_written++;
    }
    print STDERR "total written = ", $records_total, " ", $records_written, "\n";
}

sub readerFastq($) 
{
    my $filefq = $_[0];
    open(my $fh, "gunzip -c $filefq|") or die $!;
    return sub {
        my @fqrecord = ();
        my $hed = <$fh>;
        my $seq = <$fh>;
        my $opt = <$fh>;
        my $qual = <$fh>;
        if ($hed && $seq && $opt && $qual) {
            chomp($hed);
            chomp($seq);
            chomp($opt);
            chomp($qual);
            push(@fqrecord, $hed, $seq, $opt, $qual);
            return \@fqrecord;
        }
        else {
            close($fh);
        }
        return undef;
    }
}

sub qualToInt($)
{
    my $qual = $_[0];
    my $qual_length = length($qual);
    my $total = 0;
    for (my $b = 0; $b < $qual_length; $b++) {
        my $qbase = ord(substr($qual, $b, 1)) - 33;
        $total += $qbase;
    }
    return $total / $qual_length;
}


sub printFastq($)
{
    my $fq = $_[0];
    foreach my $line (@{$fq}) {
        print $line,"\n";
    }
}


sub usage($)
{
    my $message = $_[0];
    if (defined $message && length $message) {
        $message .= "\n" unless ($message =~ /\n$/);
    }
    
    my $command = $0;
    $command =~ s#^.*/##;
    
    print STDERR (
    $message,
    "usage: $command --fastq file_reads.fastq.gz --umi file_umi.fastq.gz --qual 15\n" .
    "description: concatenate UMI to FASTQ header\n" );
    
    die("\n");


}