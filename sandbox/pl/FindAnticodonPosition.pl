#!/usr/bin/perl

use warnings;
use strict;

main:{
    my $fileFa = shift;

    open(my $fh, "gunzip -c $fileFa|fasta2tbl -|") or die $!;
    while(<$fh>) {
        chomp($_);
        my ($hed, $seq) = split("\t", $_, 2);
        my $len = length($seq);
        my $codon = ($_ =~ m/-([ACGT]{3,3});/) ? $1 : "XXX";
        my @seeds = ();
        my $pos = index($seq, $codon);
        while ($pos > -1) {
            push(@seeds, $pos);
            $pos = index($seq, $codon, $pos + 1);
        }

        print $hed, "\t", $codon, "\t", $len,"\t", scalar(@seeds), "\t", join(",", @seeds),"\n";
    }
    close($fh);

}