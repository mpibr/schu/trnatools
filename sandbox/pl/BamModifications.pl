#!/usr/bin/perl

use warnings;
use strict;

sub ParseMDTag($);
sub ParseCigar($);
sub CalcCigarLenRef($);
sub CalcCigarLenQry($);
sub CalcMDLen($);

main:{
    my $fileBam = shift;
    my $fileRegions = shift;

    open(my $fh, "samtools view --regions-file $fileRegions $fileBam|") or die $!;
    while(<$fh>) {
        chomp($_);
        
        my @bam = split("\t", $_);
        my $name = $bam[2];
        my $pos = $bam[3];

        my $cigar = ParseCigar($bam[5]);
        my $qryLen = CalcCigarLenQry($cigar);
        my $refLen = CalcCigarLenRef($cigar);
        
        my ($md, $mdtag) = ParseMDTag($_);
        my $mdlen = CalcMDLen($md);

        #if (index($mdtag, '^') > 0) {
        #    print $mdtag, "\t", join(",", @{$md}), "\n";
        #    print $name, "\t", $pos, "\t", $qryLen, "\t", $refLen, "\t", $mdlen, "\t", $bam[5], "\t", $mdtag, "\n";
        #}
        if ($mdlen != $refLen) {
            print $name, "\t", $pos, "\t", $qryLen, "\t", $refLen, "\t", $mdlen, "\t", $bam[5], "\t", $mdtag, "\n";
        }
        
        ## soft clip is excluded from M start position on the query

    }
    close($fh);

}


sub CalcMDLen($) 
{
    my $mdtag = $_[0];
    my $len = 0;

    foreach my $tag (@{$mdtag}) {
        if ($tag =~ /\d+/) {
            $len += $tag;
        }
        elsif ($tag =~ /^[ACGT]/) {
            $len++;
        }
        elsif ($tag =~ /^[\^][ACGT]+/) {
            $len += length($tag) - 1;
        }
    }
    return $len;
}


sub ParseMDTag($)
{
    my $bam = $_[0];
    my $mdtag = ($_ =~ m/MD\:Z\:([0-9ACGTN^]+)/) ? $1 : '';
    my @tags = ();
    if ($mdtag) {
        for my $md (grep defined, $mdtag =~ /(\d*)([\^ACGT]*)/g) {
            push(@tags, $md) if($md);
        }
    }
    return (\@tags, $mdtag);
}


sub ParseCigar($)
{
    my $cigar = $_[0];
    my @table = ();
    for my $tag (grep defined, $cigar =~ /(\d*[MIDNSHP=X])/g) {
        my ($len, $op) = $tag =~ /(\d*)(\D*)/a;
        push(@table, [uc($op), $len]);
    }
    return \@table;
}

sub CalcCigarLenRef($)
{
    my $cigar = $_[0];
    my $lensum = 0;
    foreach my $tag (@{$cigar}) {
        my $op = $tag->[0];
        my $len = $tag->[1];
        $lensum += $len if ($op eq 'M' || $op eq 'D' || $op eq 'N' || $op eq '=' || $op eq 'X');
    }
    return $lensum;
}

sub CalcCigarLenQry($)
{
    my $cigar = $_[0];
    my $lensum = 0;
    foreach my $tag (@{$cigar}) {
        my $op = $tag->[0];
        my $len = $tag->[1];
        $lensum += $len if ($op eq 'M' || $op eq 'I' || $op eq 'S' || $op eq '=' || $op eq 'X');
    }
    return $lensum;
}