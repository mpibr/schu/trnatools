#!/usr/bin/perl

use warnings;
use strict;
use File::Basename;

sub LoadReferenceTable($);
sub WriteBedRegions($$$);
sub ParseBamFile($$$$);
sub ParseCigar($);
sub CalcCigarLenRef($);
sub CalcCigarLenQry($);
sub PrintTable($$);

main:{
    # input path to BAM files
    my $pathBam = shift;
    my @listBams = glob("$pathBam/*.bam");
    if (scalar(@listBams) == 0) {
        print "warning: no bam files found.\n";
        return;
    }

    # build reference count table from BAM header
    my $table = LoadReferenceTable($listBams[0]);
    my $fileBed = WriteBedRegions($table, $pathBam, "tempRegions");

    # count BAM files
    my @samples = ();
    foreach my $fileBam (@listBams) {
        my $name = basename($fileBam, ".bam");
        push(@samples, $name);
        my ($sample, $tmp) = split("\_", $name, 2);
        ParseBamFile($table, $fileBam, $fileBed, $name);
    }

    # clean temporary BED regions
    system("rm -rf $fileBed");

    PrintTable($table, \@samples);
}


sub PrintTable($$)
{
    my $table = $_[0];
    my $samples = $_[1];

    # print header
    print "tRNA";
    print "\tlength";
    foreach my $sample (@{$samples}) {
        print "\t",$sample,".raw";
        print "\t",$sample,".charged";
        print "\t",$sample,".uncharged";
    }
    print "\n";

    # print table
    foreach my $trna (sort keys %{$table}) {
        print $trna;
        print "\t",$table->{$trna}{"length"};
        foreach my $sample (@{$samples}) {
            my $valueRaw = exists($table->{$trna}{$sample}{"raw"}) ? $table->{$trna}{$sample}{"raw"} : 0;
            my $valueCharged = exists($table->{$trna}{$sample}{"charged"}) ? $table->{$trna}{$sample}{"charged"} : 0;
            my $valueUncharged = exists($table->{$trna}{$sample}{"uncharged"}) ? $table->{$trna}{$sample}{"uncharged"} : 0;
            print "\t", $valueRaw, "\t", $valueCharged, "\t",$valueUncharged;
        }
        print "\n";
    }
}


sub ParseBamFile($$$$)
{
    my $table = $_[0];
    my $fileBam = $_[1];
    my $fileBed = $_[2];
    my $name = $_[3];

    open(my $fh, "samtools view --regions-file $fileBed -f 16 $fileBam|") or die $!;
    while(<$fh>) {
        chomp($_);
        my @bam = split("\t", $_);
        my $cigar = ParseCigar($bam[5]);
        my $pos = $bam[3];
        my $qryLen = CalcCigarLenQry($cigar);
        my $refLen = CalcCigarLenRef($cigar);
        my $rnaLen = $table->{$bam[2]}{"length"};
        my $isTpEnd = ($pos + $refLen - 1) == $rnaLen;
        $qryLen -= $cigar->[-1][1] if ($cigar->[-1][0] eq "S");
        my $tagSoft = substr($bam[9], $qryLen);
        my $isCharged = $isTpEnd && (($tagSoft =~ m/^CCA/) || ($tagSoft =~ m/CCA$/));
        my $isUncharged = $isTpEnd && !$isCharged;

        $table->{$bam[2]}{$name}{"raw"}++;
        $table->{$bam[2]}{$name}{"charged"}++ if($isCharged);
        $table->{$bam[2]}{$name}{"uncharged"}++ if($isUncharged);
    }
    close($fh);
}


sub ParseCigar($)
{
    my $cigar = $_[0];
    my @table = ();
    for my $tag (grep defined, $cigar =~ /(\d*[MIDNSHP=X])/g) {
        my ($len, $op) = $tag =~ /(\d*)(\D*)/a;
        push(@table, [uc($op), $len]);
    }
    return \@table;
}

sub CalcCigarLenRef($)
{
    my $cigar = $_[0];
    my $lensum = 0;
    foreach my $tag (@{$cigar}) {
        my $op = $tag->[0];
        my $len = $tag->[1];
        $lensum += $len if ($op eq 'M' || $op eq 'D' || $op eq 'N' || $op eq '=' || $op eq 'X');
    }
    return $lensum;
}

sub CalcCigarLenQry($)
{
    my $cigar = $_[0];
    my $lensum = 0;
    foreach my $tag (@{$cigar}) {
        my $op = $tag->[0];
        my $len = $tag->[1];
        $lensum += $len if ($op eq 'M' || $op eq 'I' || $op eq 'S' || $op eq '=' || $op eq 'X');
    }
    return $lensum;
}

sub WriteBedRegions($$$)
{
    my $table = $_[0];
    my $path = $_[1];
    my $sample = $_[2];
    my $fileBed = $path . "/" . $sample . ".bed";
    open(my $fh, ">", $fileBed) or die $!;
    foreach my $trna (sort keys %{$table}) {
        print $fh $trna, "\t", 0, "\t", $table->{$trna}{"length"}, "\t", $trna, "\n";
    }
    close($fh);
    return $fileBed;
}


sub LoadReferenceTable($)
{
    my $fileBam = $_[0];
    my %table = ();
    open(my $fh, "samtools view -H $fileBam | grep \"tRNA\"|") or die $!;
    while (<$fh>) {
        chomp($_);
        next if($_ !~ m/^\@SQ/);
        my $key = ($_ =~ m/SN:([^\t]+)/) ? $1 : "<unknown>";
        my $len = ($_ =~ m/LN:([\d]+)/) ? $1 : "0";
        $table{$key}{"length"} = $len;
    }
    close($fh);
    return \%table;
}
