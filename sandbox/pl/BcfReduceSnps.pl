#!/usr/bin/perl

use warnings;
use strict;

sub ParseHeader($$);
sub ParseSnps($$);

main:{
    my $fileBcf = shift;
    my %table = ();
    ParseHeader(\%table, $fileBcf);
    ParseSnps(\%table, $fileBcf);
}


sub ParseSnps($$)
{
    my $table = $_[0];
    my $fileBcf = $_[1];
    open(my $fh, "bcftools view --types snps -H $fileBcf|") or die $!;
    while (<$fh>) {
        chomp($_);
        my ($key, $pos, $skip, $ref, $new, $score, $tmp, $info, @rest) = split("\t", $_, 13);
        my $len = $table->{$key};
        my $dp = ($info =~ m/DP=(\d+);/) ? $1 : 0;
        print $key,"\t",$len,"\t",$pos,"\t",$ref,"\t",$new,"\t",$score,"\t",$dp,"\n";
    }
    close($fh);
}


sub ParseHeader($$)
{
    my $table = $_[0];
    my $fileBcf = $_[1];
    open(my $fh, "bcftools view -h $fileBcf|") or die $!;
    while (<$fh>) {
        chomp($_);
        next if($_ !~ m/^##contig/);
        my $id = $_ =~ m/ID=([A-Za-z0-9\.\;\_\-]+),/ ? $1 : "unknown";
        my $len = $_ =~ m/length=(\d+)/ ? $1 : 0;
        $table->{$id} = $len;
    }
    close($fh);
}