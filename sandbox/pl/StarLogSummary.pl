#!/usr/bin/perl

use warnings;
use strict;
use File::Basename;

sub parseValues($);

main:{
    my $path = shift;
    my $tag = "seqlog.txt";
    print "sample\treads.input\treads.uniq\treads.multiloci\treads.toomany\treads.un.mismatch\treads.un.short\treads.un.other\tlength.input\tlength.mapped\n";
    foreach my $file (glob "$path/*$tag") {

        my $name = basename($file);
        my ($sample, $tmp) = split("\_", $name, 2);
        my $values = parseValues($file);

        print $sample, "\t", join("\t", @{$values}), "\n";
    }
}

sub parseValues($)
{
    my $file = $_[0];
    my @values = (0) x 9;
    open(my $fh, "<", $file) or die $!;
    while (<$fh>) {
        chomp($_);
        $values[0] = $1 if ($_ =~ m/\s+Number of input reads\s+\|\s+(\d+)/);
        $values[1] = $1 if ($_ =~ m/\s+Uniquely mapped reads number\s+\|\s+(\d+)/);
        $values[2] = $1 if ($_ =~ m/\s+Number of reads mapped to multiple loci\s+\|\s+(\d+)/);
        $values[3] = $1 if ($_ =~ m/\s+Number of reads mapped to too many loci\s+\|\s+(\d+)/);
        $values[4] = $1 if ($_ =~ m/\s+Number of reads unmapped: too many mismatches\s+\|\s+(\d+)/);
        $values[5] = $1 if ($_ =~ m/\s+Number of reads unmapped: too short\s+\|\s+(\d+)/);
        $values[6] = $1 if ($_ =~ m/\s+Number of reads unmapped: other\s+\|\s+(\d+)/);
        $values[7] = $1 if ($_ =~ m/\s+Average input read length\s+\|\s+(\d+)/);
        $values[8] = $1 if ($_ =~ m/\s+Average mapped length\s+\|\s+(\d+)/);
    }
    close($fh);
    return \@values
}
