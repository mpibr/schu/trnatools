#!/usr/bin/perl

use warnings;
use strict;

main:{
    my $samtools = shift;
    my $fileBam = shift;
    my $seed = int(rand(100));
    my @frac = ();
    my @reads = ();

    my %table = ();
    for (my $f = 0.1; $f <= 1; $f += 0.1) {
        print STDERR "fraction: ", $f,", seed: ", $seed,"\n";
        my $r = 0;
        open(my $fh, "$samtools view --subsample $f --subsample-seed $seed $fileBam|") or die $!;
        while (<$fh>) {
            chomp($_);
            $r++;
            my @bam = split("\t", $_, 12);
            next if($bam[2] !~ m/tRNA/g);
            $table{$bam[2]}{$f}++;
        }
        close($fh);
        $seed = int(rand(100));
        push(@frac, $f);
        push(@reads, $r);
    }

    # print header
    print "name";
    foreach my $f (@frac) {
        print "\t", $f;
    }
    print "\n";

    print "total";
    foreach my $r (@reads) {
        print "\t", $r;
    }
    print "\n";

    ## print table
    foreach my $key (sort keys %table) {
        print $key;
        foreach my $f (@frac) {
            my $val = exists($table{$key}{$f}) ? $table{$key}{$f} : 0;
            print "\t", $val; 
        }
        print "\n";
    }
}