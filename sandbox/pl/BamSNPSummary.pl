#!/usr/bin/perl

use warnings;
use strict;

sub LoadCodonTable($);
sub seqrc($);

main:{
    my $fileCodon = shift;
    my $fileFa = shift;

    my $tableCodons = LoadCodonTable($fileCodon);

    open(my $fh, "gunzip -c $fileFa | fasta2tbl - | grep \";tRNA\" |") or die $!;
    while(<$fh>) {
        chomp($_);
        my ($hed, $seq) = split("\t", $_, 2);
        my ($id, $name, $tmp) = split(";", $hed, 3);
        my ($aa, $codon) = split("-", $name, 2);
        #my $rc_codon = seqrc($codon);
        #$tableCodons->{$rc_codon}{"counts"}++;

        
        my @list = ();
        my $pos = index($seq, $codon);
        while ($pos > -1) {
            print $hed,"\t",$pos,"\t",$pos+3,"\n" if(30 < $pos && $pos < 40);
            push(@list, $pos);
            $pos = index($seq, $codon, $pos + 1);
        }

        #print $name, "\t", $codon, "\t", join(",", @list), "\n";
    }
    close($fh);

=head
    foreach my $codon (sort keys %{$tableCodons}) {
        my $value = exists($tableCodons->{$codon}{"counts"}) ? $tableCodons->{$codon}{"counts"} : 0;
        print $codon,"\t", 
            $tableCodons->{$codon}{"meta"}[0],"\t",
            $tableCodons->{$codon}{"meta"}[1],"\t",
            $tableCodons->{$codon}{"meta"}[2],"\t",
            $value,"\n";
    }
=cut
}

sub seqrc($)
{
    my $seq = $_[0];
    $seq =~ tr/ATGC/TACG/;
    return reverse($seq);
}


sub LoadCodonTable($)
{
    my $fileCodon = $_[0];
    my %table = ();

    open(my $fh, "<", $fileCodon) or die $!;
    while(<$fh>) {
        chomp($_);
        my ($codon, $letter, $aa, $name) = split("\t", $_, 4);
        $table{$codon}{"meta"} = [$letter, $aa, $name];
    }
    close($fh);
    return \%table;
}
