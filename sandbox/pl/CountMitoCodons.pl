#!/usr/bin/perl

use warnings;
use strict;

my %genetic_code = (
    'TCA' => 'S',    # Serine
    'TCC' => 'S',    # Serine
    'TCG' => 'S',    # Serine
    'TCT' => 'S',    # Serine
    'TTC' => 'F',    # Phenylalanine
    'TTT' => 'F',    # Phenylalanine
    'TTA' => 'L',    # Leucine
    'TTG' => 'L',    # Leucine
    'TAC' => 'Y',    # Tyrosine
    'TAT' => 'Y',    # Tyrosine
    'TAA' => '*',    # Stop
    'TAG' => '*',    # Stop
    'TGC' => 'C',    # Cysteine
    'TGT' => 'C',    # Cysteine
    'TGA' => '*',    # Stop
    'TGG' => 'W',    # Tryptophan
    'CTA' => 'L',    # Leucine
    'CTC' => 'L',    # Leucine
    'CTG' => 'L',    # Leucine
    'CTT' => 'L',    # Leucine
    'CCA' => 'P',    # Proline
    'CCC' => 'P',    # Proline
    'CCG' => 'P',    # Proline
    'CCT' => 'P',    # Proline
    'CAC' => 'H',    # Histidine
    'CAT' => 'H',    # Histidine
    'CAA' => 'Q',    # Glutamine
    'CAG' => 'Q',    # Glutamine
    'CGA' => 'R',    # Arginine
    'CGC' => 'R',    # Arginine
    'CGG' => 'R',    # Arginine
    'CGT' => 'R',    # Arginine
    'ATA' => 'I',    # Isoleucine
    'ATC' => 'I',    # Isoleucine
    'ATT' => 'I',    # Isoleucine
    'ATG' => 'M',    # Methionine
    'ACA' => 'T',    # Threonine
    'ACC' => 'T',    # Threonine
    'ACG' => 'T',    # Threonine
    'ACT' => 'T',    # Threonine
    'AAC' => 'N',    # Asparagine
    'AAT' => 'N',    # Asparagine
    'AAA' => 'K',    # Lysine
    'AAG' => 'K',    # Lysine
    'AGC' => 'S',    # Serine
    'AGT' => 'S',    # Serine
    'AGA' => 'R',    # Arginine
    'AGG' => 'R',    # Arginine
    'GTA' => 'V',    # Valine
    'GTC' => 'V',    # Valine
    'GTG' => 'V',    # Valine
    'GTT' => 'V',    # Valine
    'GCA' => 'A',    # Alanine
    'GCC' => 'A',    # Alanine
    'GCG' => 'A',    # Alanine
    'GCT' => 'A',    # Alanine
    'GAC' => 'D',    # Aspartic Acid
    'GAT' => 'D',    # Aspartic Acid
    'GAA' => 'E',    # Glutamic Acid
    'GAG' => 'E',    # Glutamic Acid
    'GGA' => 'G',    # Glycine
    'GGC' => 'G',    # Glycine
    'GGG' => 'G',    # Glycine
    'GGT' => 'G',    # Glycine
    );
    
    my %aminoacid_map = (
    'A' => 'Ala', 'R' => 'Arg', 'N' => 'Asn', 'D' => 'Asp',
    'C' => 'Cys', 'Q' => 'Gln', 'E' => 'Glu', 'G' => 'Gly',
    'H' => 'His', 'I' => 'Ile', 'L' => 'Leu', 'K' => 'Lys',
    'M' => 'Met', 'F' => 'Phe', 'P' => 'Pro', 'S' => 'Ser',
    'T' => 'Thr', 'W' => 'Trp', 'Y' => 'Tyr', 'V' => 'Val',
    'B' => 'Asx', 'Z' => 'Glx', 'X' => 'Xaa', '*' => 'Stop'
    );

sub ParseMitoFastaFile($$$);
sub PrintCodonTable($$$);

main:{
    my $fileFa = shift;
    my $fileRegion = shift;
    my %table = ();

    ParseMitoFastaFile(\%table, $fileFa, $fileRegion);
    PrintCodonTable(\%table, \%genetic_code, \%aminoacid_map);
}

sub PrintCodonTable($$$)
{
    my $codon_table_ref = $_[0];
    my $genetic_code = $_[1];
    my $aminoacid_map = $_[2];
    
    # print header
    print "#symbol\t";
    print "refseq\t";
    my @list_codons = sort keys %{$genetic_code};
    my @list_aa = ();
    my @list_abbreviation = ();
    
    foreach (@list_codons)
    {
        my $aa = $genetic_code->{$_};
        push(@list_aa, $aa);
        push(@list_abbreviation, $aminoacid_map->{$aa});
    }
    
    # sort based on AminoAcid letter
    my @index_sort = sort {$list_aa[$a] cmp $list_aa[$b]} 0..$#list_aa;
    @list_codons = @list_codons[@index_sort];
    @list_aa = @list_aa[@index_sort];
    @list_abbreviation = @list_abbreviation[@index_sort];
    
    
    print join("\t",@list_codons),"\n";
    #print "#\t#\t",join("\t", @list_aa),"\n";
    #print "#\t#\t",join("\t", @list_abbreviation),"\n";
    
    # print counts
    foreach my $gene (sort keys %{$codon_table_ref})
    {
        foreach my $transcript (sort keys %{$codon_table_ref->{$gene}})
        {
            my @count = ();
            foreach my $codon (@list_codons)
            {
                my $value = exists($codon_table_ref->{$gene}{$transcript}{$codon}) ? $codon_table_ref->{$gene}{$transcript}{$codon} : 0;
                push(@count, $value);
            }
            
            print $gene,"\t",$transcript,"\t",join("\t", @count),"\n";
        }
    }
}

sub ParseMitoFastaFile($$$)
{
    my $table = $_[0];
    my $fileFa = $_[1];
    my $fileRegion = $_[2];

    open(my $fh, "samtools faidx --region-file $fileRegion $fileFa | fasta2tbl - |") or die $!;
    while(<$fh>) {
        chomp($_);
        my ($hed, $seq) = split("\t", $_);
        my ($refseq, $symbol, $typ) = split(";", $hed, 3);
        my $seq_len = length($seq);
        for (my $idx = 0; $idx < $seq_len; $idx += 3) {
            my $codon = substr($seq, $idx, 3);
            $codon .= "AA" if (length($codon) == 1);
            $codon .= "A" if (length($codon) == 2);
            $table->{$symbol}{$refseq}{$codon}++;
        }
    }
    close($fh);
}