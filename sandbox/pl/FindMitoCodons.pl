#!/usr/bin/perl
use warnings;
use strict;

sub ParseCodonTable($$);
sub ParseMitoFastaFile($$$);
sub reversecomplement($);

sub FindCodonPosition($$$);

main:{
    my $fileFa = shift;
    my $fileRegion = shift;
    my $fileCodons = shift;
    my %codons = ();

    ParseCodonTable(\%codons, $fileCodons);
    ParseMitoFastaFile(\%codons, $fileFa, $fileRegion);

}

sub FindCodonPosition($$$)
{
    my $seq = $_[0];
    my $codons = $_[1];
    my $strand = $_[2];

    my $seq_len = length($seq);
    my $pos_exp = int(0.5 * $seq_len);
    my @seeds = ();
    foreach my $x (@{$codons}) {
        my $pos = index($seq, $x);
        while ($pos > -1) {
            my $value = abs($pos - $pos_exp);
            push(@seeds, [$x, $pos, abs($pos - $pos_exp)]) if($value <= 10);
            $pos = index($seq, $x, $pos + 1);
        }
    }

    my @norm = sort {$a->[2] <=> $b->[2]} @seeds;
    return \@norm;
}


sub ParseCodonTable($$)
{
    my $table = $_[0];
    my $fileCodons = $_[1];

    open(my $fh, "<", $fileCodons) or die $!;
    while(<$fh>) {
        my ($seq, $aa, $id, $name) = split("\t", $_, 4);
        push(@{$table->{$id}}, $seq);
    }
    close($fh);
}

sub ParseMitoFastaFile($$$)
{
    my $table = $_[0];
    my $fileFa = $_[1];
    my $fileRegion = $_[2];

    open(my $fh, "samtools faidx --region-file $fileRegion $fileFa | fasta2tbl - |") or die $!;
    while(<$fh>) {
        chomp($_);
        my ($hed, $seq) = split("\t", $_);
        my ($refseq, $symbol, $typ) = split(";", $hed, 3);
        my $codons = $table->{$symbol};

        my $idx_pos = FindCodonPosition($seq, $codons, "+");        

        print $symbol,"\t", "+", "\t",$idx_pos->[0][0], "\t", $idx_pos->[0][1],"\t", $idx_pos->[0][2],"\n" if($idx_pos->[0][0]);

        my $seqr = reversecomplement($seq);

        $idx_pos = FindCodonPosition($seqr, $codons, "-");
        print $symbol,"\t", "-", "\t",$idx_pos->[0][0], "\t", $idx_pos->[0][1],"\t", $idx_pos->[0][2],"\n" if($idx_pos->[0][0]);

        #last;
    }
    close($fh);
}


sub reversecomplement($)
{
    my $seq = $_[0];
    $seq =~ tr/ACGT/TGCA/;
    $seq = reverse($seq);
    return $seq;
}