%% OverviewSnpsPosition
clc
clear variables
close all

%% load seed position
seeds = ReadSeedPosition('../snps/tableCodonSeedPosition_Rnor6_tRNA_09Nov2020.txt');

x = (1:max(seeds.len))';
y = zeros(length(x), 1);
for i = 1 : max(seeds.len)
    y(i) = sum(seeds.pos == i);
end

figure('color','w');
h = bar(x,100.*y./sum(y));
set(h, 'EdgeColor',[105,105,105]./255, 'FaceColor',[211,211,211]./255);
set(gca,'box','off','fontsize',14,'xlim',[0,75],'ylim',[0,21]);
xlabel('tRNA nucleotide', 'fontsize', 14);
ylabel('relative occurance [%]', 'fontsize', 14);
title('observed tRNA anticodon position','fontsize',14,'fontweight','normal');
print(gcf, '-dpng', '-r300', 'figureOct21_AnticodonPosition.png');


%% observed SNPs
snps = ReadSnpsTable('../snps/bcfTableExport_142-Demethylated-snps.txt');

%% check observed positions
trnlabel = {'A>C', 'A>G', 'A>T', 'C>A', 'C>G', 'C>T',...
            'G>A', 'G>C', 'G>T', 'T>A', 'T>C', 'T>G'}';
data = zeros(max(seeds.len), length(trnlabel));
for k = 1 : length(snps.name)
    trnqry = sprintf('%s>%s', snps.ref{k},...
                              snps.alt{k});
    idx = strcmp(trnlabel, trnqry);
    data(snps.pos(k), idx) = data(snps.pos(k), idx) + snps.dp(k);
end

data = 100 .* data ./ sum(data(:));

figure('color', 'w');
h = bar(data, 'stacked');

set(h, 'edgecolor','none');
hl = legend(h, trnlabel);
set(hl,'edgecolor', 'w');
set(gca,'box','off','fontsize',14,'xlim',[0,75],'ylim',[0,21]);
xlabel('tRNA nucleotide', 'fontsize', 14);
ylabel('relative occurance [%]', 'fontsize', 14);
title('observed tRNA single nucleotide modifications','fontsize',14,'fontweight','normal');
print(gcf, '-dpng', '-r300', 'figureOct21_ModificationPosition.png');

%% function
function snps = ReadSnpsTable(fileName)
    fh = fopen(fileName, 'r');
    txt = textscan(fh, '%s %n %n %s %s %n %n', 'delimiter', '\t');
    fclose(fh);
    snps.name = txt{1};
    snps.len = txt{2};
    snps.pos = txt{3};
    snps.ref = txt{4};
    snps.alt = txt{5};
    snps.score = txt{6};
    snps.dp = txt{7};
    snps.alt = regexprep(snps.alt, ',', '');
end


function seeds = ReadSeedPosition(fileName)
    fh = fopen(fileName, 'r');
    txt = textscan(fh, '%s %s %n %n %s', 'delimiter', '\t');
    fclose(fh);
    seeds.name = txt{1};
    seeds.seq = txt{2};
    seeds.len = txt{3};
    seeds.counts = txt{4};
    seeds.ref = txt{5};
    postext = sprintf('%s,', seeds.ref{:});
    seeds.pos = sscanf(postext, '%d,');
    idx = zeros(sum(seeds.counts), 1);
    idx(cumsum(seeds.counts)+1) = 1;
    idx(1) = 1;
    seeds.idx = cumsum(idx);
end
