%% MitoCodonUsageBias
clc
clear variables
close all

%% read codon metadata
codons = ReadCodonMetadata('../../codons/codonTable.txt');

%% read codon frequency
rna = ReadCodonFrequency(codons, '../../codons/tableCodons_RatRNA_May2021.txt');
mito = ReadCodonFrequency(codons, '../../codons/tableCodons_rn7_mrbsq-mRNAs_03Dec2021.txt');

%% codon relative adaptiveness
codons.raEncoder = CodonRelativeAdaptiveness(mito.tag, mito.encoder);
codons.raEncoder(codons.raEncoder == 0) = 0.05;
%codons.raEncoder = CodonRelativeAdaptiveness(mito.tag, mito.encoder);



%% plot relative adaptiveness
idx = grp2idx(codons.tag);
data = zeros(max(idx), 6);
label = repmat({''}, max(idx), 1);

py = idx;
px = zeros(length(idx), 1);
pc = repmat({''}, length(idx), 1);
pl = repmat({''}, length(idx), 1);
for i = 1: max(idx)
    idxuse = idx == i;
    values = codons.raEncoder(idxuse);
    [values, idxsrt] = sort(values, 'descend');
    data(i,1:length(values)) = sort(values,'descend');
    label(i) = unique(codons.tag(idxuse));
    ncodons = sum(idxuse);
    %dvalues = codons.raDecoder(idxuse);
    clr = repmat({'w'},ncodons, 1);
    %clr(dvalues == 0) = {'r'};
    seq = codons.seq(idxuse);
    
    pl(idxuse) = seq(idxsrt);
    pc(idxuse) = clr(idxsrt);
    px(idxuse) = (1:ncodons);
    
end

%% read decoder
fh = fopen('../../codons/tableCodons_mt-tRNA_seeds.txt', 'r');
txt = textscan(fh,'%s %s %n %n','delimiter', '\t');
fclose(fh);
decoder.tag = txt{1};
decoder.seq = txt{2};

for k = 1 : length(decoder.seq)
    cc = strcmp(seqrcomplement(decoder.seq{k}), pl);
    if sum(cc) > 0
        pc(cc) = {'r'};
    end
end


customcolor = colormap('winter');
customcolor(1,:) = [1, 1, 1];
%customcolor(end,:) = [148,0,211]./255;
figure('color','w');
imagesc(data, [0, 1]);
colormap(customcolor);
c = colorbar;
set(c.Label, 'String', 'codon relative adaptiveness', 'FontSize',14);
xlabel('rank');
set(gca,'fontsize', 14,...
        'ytick', (1:max(idx)),...
        'yticklabels',label);
for k = 1 : length(idx)
    text(px(k), py(k), pl{k}, 'fontsize',12, 'color', pc{k},'horizontalalignment','center','verticalalignment','middle');
end

print(gcf, '-dpng', '-r300', '../../overview/plots/figureOverview_CodonUsageBias_mrbs-mRNAs_03Dec2021.png');


%% functions
function ra = CodonRelativeAdaptiveness(tag, counts)

    idx = grp2idx(tag);
    value = accumarray(idx, counts, [max(idx), 1], @max);
    value = value(idx);
    ra = counts ./ value;

end


function codons = ReadCodonFrequency(codons, fileName)

    fh = fopen(fileName, 'r');
    header = fgetl(fh);
    header = regexp(header, '\t', 'split');
    fmt = repmat({'%n'}, length(header), 1);
    fmt(1:2) = {'%s'};
    fmt = sprintf('%s ', fmt{:});
    txt = textscan(fh, fmt, 'delimiter', '\t');
    counts = [txt{3:end}];
    encoder = sum(counts, 1);
    seq = header(3:end);
    codons.encoder = zeros(length(seq), 1);
    for k = 1 : length(seq)
        aac = seq{k};
        idx = strcmp(aac, codons.seq);
        codons.encoder(idx) = encoder(k);
    end
    
end


function codons = ReadCodonMetadata(fileName)

    fh = fopen(fileName, 'r');
    txt = textscan(fh, '%s %s %s %s', 'delimiter', '\t');
    fclose(fh);
    codons.seq = txt{1};
    codons.aa = txt{2};
    codons.tag = txt{3};
    codons.name = txt{4};
    
    [~, idx] = sort(codons.tag);
    codons.seq = codons.seq(idx);
    codons.aa = codons.aa(idx);
    codons.tag = codons.tag(idx);
    codons.name = codons.name(idx);

end