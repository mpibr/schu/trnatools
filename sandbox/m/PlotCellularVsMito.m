%% PlotCellularVsMito
clc
clear vars
close all


fh = fopen('../test/tableCompareExpression_CLvsMT_tRNA.txt','r');
txt = textscan(fh, '%s %n %n', 'delimiter', '\t');
fclose(fh);
tag = txt{1};
counts = [txt{2:end}];
nc = bsxfun(@rdivide, counts, max(counts));


[y,idx] = sort(nc(:,2),'ascend');
l = tag(idx);

figure('color','w');
h = barh(y);
set(h, 'EdgeColor','none','FaceColor',[255,69,0]./255);
set(gca,'ytick',(1:length(l)),...
        'yticklabel',l,...
        'fontsize',12,...
        'box','off');
xlabel('relative abundance','fontsize', 14);
title('mito tRNAs','fontsize',14,'fontweight','normal');
print(gcf,'-dpng','-r300','../overview/plots/figureNov01_Abundance_mt-tRNA.png');