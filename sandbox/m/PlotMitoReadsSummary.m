%% PlotMitoReadsSummary
clc
clear vars
close all

fh = fopen('../test/summary_t142B-09.txt', 'r');
txt = textscan(fh,'%s %n','delimiter','\t');
fclose(fh);
rna.type = txt{1};
rna.count = txt{2};
rna.r = 100.*rna.count./sum(rna.count);

figure('color','w');
h = barh(flipud(rna.r));
set(h,'edgecolor','none','facecolor',[255,140,0]./255);
set(gca,'ytick',(1:length(rna.type)),...
        'yticklabel',flipud(rna.type),...
        'fontsize',12,...
        'box','off');
xlabel('relative abundance [%]','fontsize',14);
text(10,7, '459.713 reads', 'fontsize',14,'verticalalignment','middle','horizontalalignment','left');
text(10,12, '43.108.391 reads', 'fontsize',14,'verticalalignment','middle','horizontalalignment','left');

print(gcf,'-dpng','-r300','../overview/plots/figureNov01_ReadsSummary.png');