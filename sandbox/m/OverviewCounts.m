%% OverviewCounts
clc
clear variables
close all


%% read data table
table = ReadDataTable('data/tableCounts_tRNAs_15Sep2021.txt');

%% choose samples
sample = {'t142B-09'; 't142B-11'; 't142C-06'; 't142C-08'};
idxSample = contains(table.header, sample);
idxRaw = contains(table.header, '.raw');
idxCharged = contains(table.header, '.charged');
idxUncharged = contains(table.header, '.uncharged');

countsRaw = table.counts(:, idxSample & idxRaw);
countsCharged = table.counts(:, idxSample & idxCharged);
countsUncharged = table.counts(:, idxSample & idxUncharged);


%% plot rank list
%{
idxFilter = all(countsRaw > 0, 2);
y = log2(mean(countsRaw(idxFilter,:), 2));
l = table.names(idxFilter);

[y, idxSort] = sort(y, 'descend');
l = l(idxSort);
x = (1:length(y))';
idxTRNA = contains(l, ';tRNA');

figure('color', 'w');
h(1) = plot(x(~idxTRNA), y(~idxTRNA), '.', 'color', [.65,.65,.65]);
hold on;
h(2) = plot(x(idxTRNA), y(idxTRNA), '.', 'color', [30,144,255]./255, 'markersize', 10);
hold off;
set(gca, 'xlim', [-500, length(l)+500], ...
         'ylim', [0, 21], ...
         'box', 'off', ...
         'fontsize', 12);
xlabel('rank order', 'fontsize', 14);
ylabel('# reads [log_2]', 'fontsize', 14);
hl = legend(h, 'RNAs', 'tRNAs');
set(hl, 'edgecolor', 'w', 'fontsize', 12, 'location', 'east');

lblA = l(~idxTRNA);
lblA = regexp(lblA(1:14), ';', 'split');
[lblA, idxA] = unique(cellfun(@(x) x(2), lblA));
[~, idxA] = sort(idxA);
lblA = sprintf('%s\n',lblA{idxA});

lblB = l(idxTRNA);
lblB = regexp(lblB(1:30), ';', 'split');
[lblB, idxB] = unique(cellfun(@(x) x(2), lblB));
[~, idxB] = sort(idxB);
lblB = sprintf('%s\n',lblB{idxB});

text(2000, 20, lblA, 'fontsize', 10, 'horizontalalignment', 'left', 'verticalalignment','top');
text(6000, 20, lblB, 'fontsize', 10, 'horizontalalignment', 'left', 'verticalalignment','top');
print(gcf, '-dpng', '-r300', 'overview/figureOverview_RankOrder_16Sep2021.png');
%}

%% plot charged
idxFilter = all(countsRaw > 0, 2);
y = log2(mean(countsRaw(idxFilter,:), 2));
l = table.names(idxFilter);
r = 100 .* countsCharged(idxFilter,:) ./ countsRaw(idxFilter,:);
r = mean(r, 2);
idxNoZero = r > 0;
idxTRNA = contains(l, ';tRNA');

y = y(idxNoZero & idxTRNA);
l = l(idxNoZero & idxTRNA);
r = r(idxNoZero & idxTRNA);

l = regexp(l, ';', 'split');
l = cellfun(@(x) x(2), l);

%
figure('color', 'w');
plot(y, r, '.', 'color', [30,144,255]./255, 'markersize', 10);
set(gca, 'box', 'off',...
         'fontsize', 12);
xlabel('# reads [log_2]', 'fontsize', 14);
ylabel('fraction charged [%]', 'fontsize', 14);
%gname(l);
%print(gcf, '-dpng', '-r300', 'overview/figureOverview_ChargeRatio_16Sep2021.png');
%}

%{
lx = regexp(l, '-', 'split');
lAA = cellfun(@(x) x(1), lx);
lCDN = cellfun(@(x) x(2), lx);


l = l;


idxGrp = grp2idx(l);
figure('color', 'w');
h = plotSpread(y, 'distributionIdx', l, 'distributionColor', [.65,.65,.65], 'xyOri', 'flipped');
set(h{3}, 'fontsize', 8);

hold on;
w = 0.25;
for k = 1 : max(idxGrp)
    xm = median(y(idxGrp == k));
    xl = prctile(y(idxGrp == k), 25);
    xu = prctile(y(idxGrp == k), 75);
    plot([xl, xu], [k, k], 'r');
    plot([xm,xm],[k-w,k+w],'r','linewidth',2);
end
hold off;

xlabel('# reads [log_2]', 'fontsize', 14);
print(gcf, '-dpng', '-r300', 'overview/figureOverview_CDNperReads_16Sep2021.png');


figure('color', 'w');
h = plotSpread(r, 'distributionIdx', l, 'distributionColor', [.65,.65,.65], 'xyOri', 'flipped');
set(h{3}, 'fontsize', 8);
hold on;
w = 0.25;
for k = 1 : max(idxGrp)
    xm = median(r(idxGrp == k));
    xl = prctile(r(idxGrp == k), 25);
    xu = prctile(r(idxGrp == k), 75);
    plot([xl, xu], [k, k], 'r');
    plot([xm,xm],[k-w,k+w],'r','linewidth',2);
end
hold off;
xlabel('fraction charged [%]', 'fontsize', 14);
print(gcf, '-dpng', '-r300', 'overview/figureOverview_CDNperCharge_16Sep2021.png');
%}
%% functions
function table = ReadDataTable(fileName)

    fh = fopen(fileName, 'r');
    hdr = fgetl(fh);
    hdr = regexp(hdr, '\t', 'split');
    
    fmt = repmat({'%n'}, length(hdr), 1);
    fmt(1) = {'%s'};
    fmt = sprintf('%s',fmt{:});
    txt = textscan(fh, fmt, 'delimiter', '\t');
    fclose(fh);
    table.header = hdr(3:end);
    table.names = txt{1};
    table.len = txt{2};
    table.counts = [txt{3:end}];
end