%% OverviewBcfSnps
clc
clear variables
close all


%% read codon metadata
codons = ReadCodonMetadata('../codons/codonTable.txt');

%% read codon frequency
codons = ReadCodonFrequency(codons, '../codons/tableCodons_RatRNA_May2021.txt');

%% read decoder annotation
codons = ReadDecoderAnnotation(codons, '../snps/codonTable_annotations_09Nov2020.txt');


%% annotated transitions
transitions.label = {'A>C', 'A>G', 'A>T', 'C>A', 'C>G', 'C>T',...
                     'G>A', 'G>C', 'G>T', 'T>A', 'T>C', 'T>G'}';
transitions.ratios = zeros(12, 2);

seqtag = char(codons.seq);
idx = grp2idx(strcat(codons.tag, '.', cellstr(seqtag(:, 1:2))));
for k = 1 : max(idx)
    
    idxuse = idx == k;
    values = codons.decoder(idxuse);
    base = seqtag(idxuse, 3);
    
    if (length(values) > 1) && (sum(values == 0) == 1)
        idxZero = values == 0;
        distZero = values - values(idxZero);
        baseZero = strcat(base, '>', base(idxZero));
        [~,idxA, idxB] = intersect(transitions.label, cellstr(baseZero));
        transitions.ratios(idxA, 1) = transitions.ratios(idxA, 1) + distZero(idxB);
    end
    
end



%% observed transitions
snps = ReadSnpsTable('../snps/bcfTableExport_142-Demethylated-snps.txt');
for k = 1 : length(snps.name)
    qtrn = strcat(seqcomplement(snps.ref{k}),...
                  '>',...
                  seqcomplement(snps.alt{k}));
    idxuse = strcmp(transitions.label, qtrn);
    transitions.ratios(idxuse, 2) = transitions.ratios(idxuse, 2) + snps.dp(k);
end


%% plot transitions
%{
nrmratios = bsxfun(@rdivide, 100 .* transitions.ratios, sum(transitions.ratios));
figure('color', 'w');
h = bar(nrmratios);
set(h(1), 'EdgeColor', 'none', 'FaceColor', [30,144,255]./255);
set(h(2), 'EdgeColor', 'none', 'FaceColor', [250,128,114]./255);
hl = legend(h, 'annotated', 'observed');
set(hl, 'edgecolor','w', 'location','northwest','fontsize',14);
set(gca, 'box','off', 'fontsize', 14,...
    'xticklabel', transitions.label,...
    'xticklabelrotation', 30);
xlabel('single nucleotide polymorphism', 'fontsize', 14);
ylabel('relative occurance [%]', 'fontsize', 14);
print(gcf, '-dpng', '-r300', 'figureOct21_CodonSnps.png');
%}

%% positional seed transitions
fh = fopen('../snps/tableCodonSeedPosition.txt', 'r');
txt = textscan(fh, '%n', 'delimiter', '\t');
fclose(fh);
seedPosition = txt{1};

[seedFreq, seedEdges] = histcounts(seedPosition, 25);
seedCenters = seedEdges(1:end-1) + diff(seedEdges)./2;

figure('color','w');
bar(seedCenters, 100.*seedFreq./sum(seedFreq));

%% positional transitions
%
nbins = length(seedEdges) - 1;
postrans = zeros(nbins, 12);
for k = 1 : length(snps.name)
    qtrn = strcat(seqcomplement(snps.ref{k}),...
                  '>',...
                  seqcomplement(snps.alt{k}));
    x = (snps.pos(k) + 3) / snps.len(k);
    idxlbl = strcmp(transitions.label, qtrn);
    idxbin = find(seedEdges <= x, 1, 'last');
    if (idxbin <= nbins)
    postrans(idxbin, idxlbl) = postrans(idxbin, idxlbl) + snps.dp(k);
    end
    
end
npostrans = 100 .* postrans ./ sum(postrans(:));
%
figure('color', 'w');
h = bar(npostrans, 'stacked');
hl = legend(h, transitions.label);
set(hl,'edgecolor', 'w');
%set(gca, 'xticklabel', round(unique(diff(edges)./2)*get(gca,'xtick')));
%}

%% functions
function snps = ReadSnpsTable(fileName)

    fh = fopen(fileName, 'r');
    txt = textscan(fh, '%s %n %n %s %s %n %n', 'delimiter', '\t');
    fclose(fh);
    snps.name = txt{1};
    snps.len = txt{2};
    snps.pos = txt{3};
    snps.ref = txt{4};
    snps.alt = txt{5};
    snps.score = txt{6};
    snps.dp = txt{7};
    
    snps.alt = regexprep(snps.alt, ',', '');
    
end


function codons = ReadDecoderAnnotation(codons, fileName)

    fh = fopen(fileName, 'r');
    txt = textscan(fh, '%s %s %s %s %n', 'delimiter', '\t');
    seq = txt{1};
    decoder = txt{5};
    codons.decoder = zeros(length(seq), 1);
    for k = 1 : length(seq)
        aac = seq{k};
        idx = strcmp(aac, codons.seq);
        codons.decoder(idx) = decoder(k);
    end

end


function codons = ReadCodonFrequency(codons, fileName)

    fh = fopen(fileName, 'r');
    header = fgetl(fh);
    header = regexp(header, '\t', 'split');
    fmt = repmat({'%n'}, length(header), 1);
    fmt(1:2) = {'%s'};
    fmt = sprintf('%s ', fmt{:});
    txt = textscan(fh, fmt, 'delimiter', '\t');
    counts = [txt{3:end}];
    encoder = sum(counts, 1);
    seq = header(3:end);
    codons.encoder = zeros(length(seq), 1);
    for k = 1 : length(seq)
        aac = seq{k};
        idx = strcmp(aac, codons.seq);
        codons.encoder(idx) = encoder(k);
    end
    
end


function codons = ReadCodonMetadata(fileName)

    fh = fopen(fileName, 'r');
    txt = textscan(fh, '%s %s %s %s', 'delimiter', '\t');
    fclose(fh);
    codons.seq = txt{1};
    codons.aa = txt{2};
    codons.tag = txt{3};
    codons.name = txt{4};
    
    [~, idx] = sort(codons.tag);
    codons.seq = codons.seq(idx);
    codons.aa = codons.aa(idx);
    codons.tag = codons.tag(idx);
    codons.name = codons.name(idx);

end
