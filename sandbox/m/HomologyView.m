%% HomologyView
%
% tRNA homology based on tcoffee similarity
clc
clear variables
close all

%% read distance matrix
fileName = '../../test/tcoffee-rn7-tRNAs.txt';
fh = fopen(fileName, 'r');
line = fgetl(fh);
line = regexp(line, '\t', 'split');
fmt = repmat({'%n'}, length(line), 1);
fmt(1) = {'%s'};
fmt = sprintf('%s ', fmt{:});
fmt(end) = [];
frewind(fh);
txt = textscan(fh, fmt, 'delimiter', '\t');
fclose(fh);
labels = txt{1};
distmtx = [txt{2:end}];

%% label to groups
grp = regexp(labels, '\_', 'split');
pre = cellfun(@(x) x(3), grp);
pre = regexprep(pre, 'tRNA', '');
grp = cellfun(@(x) x(2), grp);
grp = regexp(grp, '\-', 'split');
grp_aa = cellfun(@(x) x(1), grp);
grp_cdn = cellfun(@(x) x(2), grp);
grp_cdn = strcat(pre,grp_aa,'-',grp_cdn);
grp_aa = cellfun(@(x) x(1), regexp(grp_cdn, '\-', 'split'));
[idx_aa, lbl_aa] = grp2idx(grp_aa);
[idx_cdn, lbl_cdn] = grp2idx(grp_cdn);
grp = grp_cdn;
grp(strncmp('mt',grp_aa,2)) = {'mt-tRNA'};
[idx_grp, lbl_grp] = grp2idx(grp);

%% between and within clusters
%[Y, e] = cmdscale(100 - distmtx);
%{
idx = idx_aa;
lbl = lbl_aa;
k = 2;
ytick = zeros(max(idx), 1);
for k = 1 : max(idx)
    
    q = find(idx == k);
    ytick(k) = min(q) + (max(q) - min(q)) / 2;
end


figure('color','w');
imagesc(distmtx);
set(gca,'XTickLabel','');
set(gca,'ytick', ytick);
set(gca,'YTickLabel', lbl);
set(gca,'fontsize', 6);
%}
Y = squareform(100 - distmtx, 'tovector');
Z = linkage(Y, 'complete');
leafOrder = optimalleaforder(Z,Y);
h = dendrogram(Z, 0, 'Reorder', leafOrder);
%set(h,'Color','k');
%set(h(1),'Color','g');


clf
polardendrogram(Z,0,'colorthreshold','default', 'reorder', leafOrder);
h = get(gca,'Children');
set(h,'color','k');
set(gcf,'color','w');
zoom(0.8);
view(2);
