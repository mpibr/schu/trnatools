%% OverviewCodons
clc
clear variables
close all

%% read tables
tableMeta = ReadCodonMetaTable('codons/codonTable.txt');
tableCodons = ReadCodonsTable('codons/tableCodons_RatRNA_May2021.txt');
tableExpression = ReadGeneExpression('codons/tableExpression_May2021.txt');
table = ReadDataTable('data/tableCounts_tRNAs_15Sep2021.txt');

%% match codons and expression
[~, idxA, idxB] = intersect(tableCodons.transcript, tableExpression.transcript);
subtableCodons.transcript = tableCodons.transcript(idxA);
subtableCodons.counts = tableCodons.counts(idxA, :);
subtableExpression.transcript = tableExpression.transcript(idxB);
subtableExpression.counts = tableExpression.counts(idxB,:);

%% DNA weight
wDNA = sum(subtableCodons.counts) ./ sum(subtableCodons.counts(:));
wRNA = mean(subtableExpression.counts(:,1:2), 2);
wRNA = subtableCodons.counts .* wRNA;
wRNA = sum(wRNA) ./ sum(wRNA(:));
wFPR = mean(subtableExpression.counts(:,3:4), 2);
wFPR = subtableCodons.counts .* wFPR;
wFPR = sum(wFPR) ./ sum(wFPR(:));

nCodons = length(tableCodons.codons);
G = repmat({'X'}, nCodons, 1);
for i = 1 : nCodons
    idx = strcmp(tableCodons.codons{i}, tableMeta.codons);
    G(i) = tableMeta.id(idx);
end


%% plot weights
%{
x = wRNA;
y = wFPR;

mdl = fitlm(x, y);
yfit = feval(mdl, x);

t_cookd = 3*mean(mdl.Diagnostics.CooksDistance');
idxOutliers = find(mdl.Diagnostics.CooksDistance > t_cookd);

figure('color', 'w');
hold on;
plot(x, yfit, 'k');
h = gscatter(x, y, G);
plot(x(idxOutliers), y(idxOutliers), 'ro', 'MarkerSize', 12);
hold off;
hl = legend(h, 'edgecolor', 'w', 'location','eastoutside');
set(gca, 'box', 'off', 'fontsize', 12);
xlabel('codon frequency [RNA]', 'fontsize',14);
ylabel('codon frequency [RIBO]', 'fontsize',14);
text(0.001, 0.05, sprintf('adj.R^2 = 0.937\np-value = 3.25e-39'),'horizontalalignment','left','verticalalignment','top');
print(gcf, '-dpng', '-r300', 'overview/figureOverview_CodonFreq_RNAvsRIBO_17Sep2021.png');
%}


%% compare to data
sample = {'t142B-09'; 't142B-11'; 't142C-06'; 't142C-08'};
idxSample = contains(table.header, sample);
idxRaw = contains(table.header, '.raw');
idxCharged = contains(table.header, '.charged');
idxUncharged = contains(table.header, '.uncharged');

countsRaw = table.counts(:, idxSample & idxRaw);
countsCharged = table.counts(:, idxSample & idxCharged);
countsUncharged = table.counts(:, idxSample & idxUncharged);

lbl = regexp(table.names, ';', 'split');
lbl = cellfun(@(x) x(2), lbl);

counts = countsRaw;
k = 4;
data = zeros(nCodons, 1);
for k = 1 : nCodons
    qry = sprintf('%s-%s', G{k}, seqrcomplement(tableCodons.codons{k}));
    idx = strcmp(lbl, qry);
    if sum(idx) > 0
        data(k) = log2(max(mean(counts(idx,:), 2)));
    end
end
%data = data ./ sum(data);

idxFilter = data > 0;
x = wFPR(idxFilter);
y = data(idxFilter);
grp = G(idxFilter);

mdl = fitlm(x, y);
yfit = feval(mdl, x);

t_cookd = 3*mean(mdl.Diagnostics.CooksDistance');
idxOutliers = find(mdl.Diagnostics.CooksDistance > t_cookd);

figure('color', 'w');
hold on;
plot(x, yfit, 'k');
h = gscatter(x, y, grp);
%plot(x(idxOutliers), y(idxOutliers), 'ro', 'MarkerSize', 12);
hold off;
hl = legend(h, 'edgecolor', 'w', 'location','eastoutside');
set(gca, 'box', 'off', 'fontsize', 12);
xlabel('codon frequency [RIBO]', 'fontsize',14);
ylabel('tRNA expression [log_2 # reads]', 'fontsize',14);
text(0.03, 8, sprintf('adj.R^2 = %.3f\np-value = %.2E', ...
    mdl.Rsquared.Adjusted, mdl.Coefficients.pValue(2)),...
    'horizontalalignment','left','verticalalignment','top');
%print(gcf, '-dpng', '-r300', 'overview/figureOverview_CorrelationUncharged_RIBOvsTRNA_17Sep2021.png');

%% functions
function table = ReadGeneExpression(fileName)
    fh = fopen(fileName, 'r');
    hdr = fgetl(fh);
    hdr = regexp(hdr, '\t', 'split');
    fmt = repmat({'%n'}, length(hdr), 1);
    fmt(1:2) = {'%s'};
    fmt = sprintf('%s',fmt{:});
    txt = textscan(fh, fmt, 'delimiter', '\t');
    fclose(fh);
    table.header = hdr(4:end)';
    table.gene = txt{1};
    table.transcript = txt{2};
    table.len = txt{3};
    table.counts = [txt{4:end}];
end


function table = ReadCodonsTable(fileName)
    fh = fopen(fileName, 'r');
    hdr = fgetl(fh);
    hdr = regexp(hdr, '\t', 'split');
    fmt = repmat({'%n'}, length(hdr), 1);
    fmt(1:2) = {'%s'};
    fmt = sprintf('%s',fmt{:});
    txt = textscan(fh, fmt, 'delimiter', '\t');
    fclose(fh);
    table.codons = hdr(3:end)';
    table.gene = txt{1};
    table.transcript = txt{2};
    table.counts = [txt{3:end}];
end


function table = ReadCodonMetaTable(fileName)
    fh = fopen(fileName, 'r');
    txt = textscan(fh, '%s %s %s %s', 'delimiter', '\t');
    table.codons = txt{1};
    table.AA = txt{2};
    table.id = txt{3};
    table.name = txt{4};
    fclose(fh);
end


function table = ReadDataTable(fileName)

    fh = fopen(fileName, 'r');
    hdr = fgetl(fh);
    hdr = regexp(hdr, '\t', 'split');
    
    fmt = repmat({'%n'}, length(hdr), 1);
    fmt(1) = {'%s'};
    fmt = sprintf('%s',fmt{:});
    txt = textscan(fh, fmt, 'delimiter', '\t');
    fclose(fh);
    table.header = hdr(3:end);
    table.names = txt{1};
    table.len = txt{2};
    table.counts = [txt{3:end}];
end