# %% libraries
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# %% load amino acids tables
snps = pd.read_csv("../snps/tableST_revr_Sep2021.txt", delimiter='\t', header=None)
data = pd.read_csv("../snps/codonTable_annotations_09Nov2020.txt", delimiter='\t', header=None)

snps.rename(columns = {0:"snp", 1:"observed", 2:"seeds"}, inplace = True)
snps['annotation'] = 0
del snps['seeds']

# %% accumulate list of transitions
key = data[2] + data[0].apply(lambda x: x[0:2])

for k in key.unique():
    idx = key == k
    df = data[idx].sort_values(by=4)
    base = df[0].apply(lambda x: x[-1])
    bnext = list(base)[0]
    for b in base:
        dkey = b + ">" + bnext
        value = list(df[b==base][4])[0] # / df[4].sum())[0]
        idxloc = snps['snp'].str.fullmatch(dkey)
        if idxloc.any():
            snps.loc[idxloc, 'annotation'] = snps[idxloc]['annotation'] + value
        #print(dkey, " ", value)
    


# %% plot heatmap
lables = snps['snp']
#observed = 100 * snps['observed'] / snps['observed'].sum()
#expected = 100 * snps['annotation'] / snps['annotation'].sum()
observed = snps['observed']
expected = snps['annotation']


fig = plt.figure()
ax = fig.add_axes([0,0,1,1])
ax.bar(observed.index - 0.2, observed.values, width = 0.4, color = 'k')
ax.bar(expected.index + 0.2, expected.values, width = 0.4, color = 'darkgray')
ax.legend(labels = ['observed', 'annotated'], loc = 'upper left', frameon = False)
ax.set_xticks(observed.index)
ax.set_xticklabels(snps['snp'])
ax.set_ylabel('# of events')
ax.set_xlabel('single nucleotide transitions')




# %% save to file
