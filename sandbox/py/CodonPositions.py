# %% load libraries
import gzip
from Bio import SeqIO
from matplotlib import pyplot as plt
import numpy


# %%
x = []
with gzip.open("../snps/Rnor6_tRNA_09Nov2020.fna.gz", "rt") as handle:
    for record in SeqIO.parse(handle, "fasta"):
        name = record.id
        name = name.split(";")[1]
        codon = name.split("-")[1]
        seqlength = len(record.seq)
        pos = record.seq.find(codon)
        while pos > 0:
            x.append(pos/seqlength)
            print(pos/seqlength)
            pos = record.seq.find(codon, pos + 1)
            

        
        

# %%
#fig, ax = plt.subplots(1,1)
#ax.hist(x, bins = 25, color='#00A2FF')
#ax.set_xticks([0, 0.25, 0.5, 0.75, 1.0])
#ax.set_xlabel("relative tRNA position")
#ax.set_ylabel("# of events")

# %%
