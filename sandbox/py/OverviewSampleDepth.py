# %% load libraries
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt

# %% load data
data = pd.read_csv('../test/tableSubsampleDepth_14Oct2021.txt', sep='\t', index_col=0, header=0)

# %%
total = data.iloc[0,:].values
reads = data.iloc[1:,:].sum()

# %%
