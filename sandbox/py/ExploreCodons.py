# %%
import numpy as np
import pandas as pd

# %%
## read data
codons = pd.read_csv('tableCodons_RatRNA_May2021.txt', sep='\t', lineterminator='\n', index_col=1)
rates = pd.read_csv('tableExpression_May2021.txt', sep='\t', lineterminator='\n', index_col=1)

# %%
## splice dataframe
data = codons.loc[rates.index]

# %%
