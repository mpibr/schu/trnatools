# %% load libraries
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

# %% load codon frequencies
data = pd.read_csv('../codons/tableCodons_RatRNA_May2021.txt', sep = '\t', lineterminator = '\n', index_col = 1)
data = data.drop('gene', axis = 1)

# %% observed codons frequency
fcodons = data.sum()

# %% load codon summary
codons = pd.read_csv('../codons/codonTable.txt', sep = '\t', lineterminator = '\n', header = None, index_col = 0)
codons = codons.merge(fcodons.to_frame(), left_index = True, right_index = True)

# %% relative adaptiveness
x = pd.Series(dtype='float64')
for aa in codons[2].unique():
    idx = aa == codons[2]
    nrm = codons[idx]
    tmp = nrm[0] / nrm[0].max()
    x = x.append(tmp)
codons = codons.merge(x.to_frame(), left_index=True, right_index=True)

# %%
q = codons['0_y']

# %%
