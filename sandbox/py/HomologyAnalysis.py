# %% 
import matplotlib.pyplot as plt
from Levenshtein import distance as lvndist
import pysam
import numpy as np
from scipy.cluster import hierarchy


# %%
d = lvndist("alabala", "alaportala")
print("distance ", d)

# %%
faObj = pysam.FastaFile("/Users/tushevg/Desktop/tRNAs/test/rn7-tRNAs.fa")

# %%
n = len(faObj.references)
dmtx = np.zeros((n, n))
lbl = []
i = 0
for ref_hed  in faObj.references:

    ref_hed_split = ref_hed.split("-")
    ref_aa = ref_hed_split[1]
    ref_cdn = ref_hed_split[2]

    print(ref_hed)
    ref_seq = faObj.fetch(ref_hed)
    ref_len = len(ref_seq)

    ref_key = ref_aa + "-" + ref_cdn + "-" + str(ref_len)
    lbl.append(ref_key)

    j = 0
    for qry_hed in faObj.references:
        qry_seq = faObj.fetch(qry_hed)
        dmtx[i, j] = lvndist(ref_seq, qry_seq)
        j = j + 1
    i = i + 1



# %%
Z = hierarchy.linkage(dmtx, 'complete', optimal_ordering=True)
plt.figure()
dn = hierarchy.dendrogram(Z, color_threshold=200, orientation='left', labels=lbl, leaf_font_size=4)

# %%
